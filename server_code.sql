-- ----------------------Server Code-----------------------------------

-- ----------------------Create Table Queries-----------------------------------
CREATE TABLE `Institution` (
    `institutionID` INT(6) NOT NULL,
    `institutionName` VARCHAR(70) NOT NULL,
    `city` VARCHAR(25) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

CREATE TABLE `AccredatedBy` (
    `institutionID` INT(6) NOT NULL,
    `accredCode` VARCHAR(8) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

CREATE TABLE `AccredatingAgency` (
    `accredCode` VARCHAR(8) NOT NULL,
    `accredAgency` VARCHAR(110) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

CREATE TABLE `InstitutionInformation` (
    `institutionID` INT(6) NOT NULL,
    `url` TEXT NOT NULL,
    `npcUrl` TEXT DEFAULT NULL,
    `mainCampus` CHAR(5) NOT NULL,
    `numBranch` INT(3) DEFAULT NULL,
    `governanceStructure` CHAR(18) NOT NULL,
    `affiliation` INT(6) DEFAULT NULL,
    `admissionRate` INT(3) DEFAULT NULL,
    `totalAdmissions` FLOAT DEFAULT NULL,
    `pctPartTimeAdmissions` FLOAT DEFAULT NULL,
    `completionRate` FLOAT DEFAULT NULL,
    `avgFacultySalary` INT(6) DEFAULT NULL,
    `rating` INT(2) DEFAULT NULL,
    `ranking` INT(4) DEFAULT NULL,
    `onCampusHousing` VARCHAR(13) DEFAULT NULL,
    `employeeSatisfaction` INT(2) DEFAULT NULL,
    `transportFacility` VARCHAR(13) DEFAULT NULL,
    `numReviews` INT(5) DEFAULT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

CREATE TABLE `Expenses` (
    `institutionID` INT(6) NOT NULL,
    `totalFee` FLOAT DEFAULT NULL,
    `tuitionFeeInState` FLOAT DEFAULT NULL,
    `tuitionFeeOutState` FLOAT DEFAULT NULL,
    `bookSupplies` FLOAT DEFAULT NULL,
    `housing` FLOAT DEFAULT NULL,
    `miscellaneous` FLOAT DEFAULT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

CREATE TABLE `InstitutionType` (
    `institutionID` INT(6) NOT NULL,
    `menOnly` CHAR(5) DEFAULT NULL,
    `womenOnly` CHAR(5) DEFAULT NULL,
    `distanceOnly` CHAR(5) DEFAULT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

CREATE TABLE `InstitutionDegree` (
    `institutionID` INT(6) NOT NULL,
    `programmeCode` INT(4) NOT NULL,
    `degreeTypeLevel` INT(2) NOT NULL,
    `placementSalaryYr1` INT DEFAULT NULL,
    `placementSalaryYr2` INT DEFAULT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

CREATE TABLE `ProgrammeDetails` (
    `programmeCode` INT(4) NOT NULL,
    `programmeDesc` VARCHAR(200) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

 CREATE TABLE `DegreeDetails` (
    `degreeTypeLevel` INT(2) NOT NULL,
    `degreeTypeDesc` VARCHAR(50) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

 CREATE TABLE `User` (
    `id` INT NOT NULL,
    `username` VARCHAR(10) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `role` VARCHAR(10) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;


 CREATE TABLE `InstitutionUser` (
    `institutionID` INT(6) NOT NULL,
    `id` INT NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

CREATE TABLE `Country` (
    `countryId` INT NOT NULL,
    `countryName` VARCHAR(40) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI; 

 CREATE TABLE `UserProfile` (
    `id` INT NOT NULL,
    `email` VARCHAR(40) NOT NULL,
    `firstName` VARCHAR(100) DEFAULT NULL,
    `lastName` VARCHAR(100) DEFAULT NULL,
    `createdOn` DATETIME DEFAULT NULL,
    `updatedOn` DATETIME DEFAULT NULL,
    `country` VARCHAR(25) DEFAULT NULL,
    `city` VARCHAR(25) DEFAULT NULL,
    `state` VARCHAR(30) DEFAULT NULL,
    `zipCode` VARCHAR(20) DEFAULT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;


 CREATE TABLE `InstRepresentative` (
    `id` INT NOT NULL,
    `institutionID` INT(6) NOT NULL,
    `institutionName` VARCHAR(70) NOT NULL
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

-- ----------------------Create Primary Keys Queries-----------------------------------

ALTER TABLE Institution ADD CONSTRAINT PK_Institution PRIMARY KEY (institutionID);
ALTER TABLE AccredatedBy ADD CONSTRAINT PK_AccredatedBy PRIMARY KEY (institutionID,accredCode);
ALTER TABLE AccredatingAgency ADD CONSTRAINT PK_AccredatingAgency PRIMARY KEY (accredCode);
ALTER TABLE InstitutionInformation ADD CONSTRAINT PK_InstitutionInformation PRIMARY KEY (institutionID);
ALTER TABLE Expenses ADD CONSTRAINT PK_Expenses PRIMARY KEY (institutionID);
ALTER TABLE InstitutionType ADD CONSTRAINT PK_InstitutionType PRIMARY KEY (institutionID);
ALTER TABLE InstitutionDegree ADD CONSTRAINT PK_InstitutionDegree PRIMARY KEY (institutionID,degreeTypeLevel,programmeCode);
ALTER TABLE ProgrammeDetails ADD CONSTRAINT PK_ProgrammeDetails PRIMARY KEY (programmeCode);
ALTER TABLE DegreeDetails ADD CONSTRAINT PK_DegreeDetails PRIMARY KEY (degreeTypeLevel);
ALTER TABLE User ADD CONSTRAINT PK_User PRIMARY KEY (id);	
ALTER TABLE InstitutionUser ADD CONSTRAINT PK_InstitutionUser PRIMARY KEY (institutionID,id);
ALTER TABLE Country ADD CONSTRAINT PK_Country PRIMARY KEY (countryId);
ALTER TABLE UserProfile ADD CONSTRAINT PK_UserProfile PRIMARY KEY (id);
ALTER TABLE InstRepresentative ADD CONSTRAINT PK_InstRepresentative PRIMARY KEY (id, institutionID);

-- ----------------------Create Foreign Keys Queries-----------------------------------

ALTER TABLE AccredatedBy ADD FOREIGN KEY (institutionID) REFERENCES Institution (institutionID);
ALTER TABLE AccredatedBy ADD FOREIGN KEY (accredCode) REFERENCES	AccredatingAgency (accredCode);
ALTER TABLE InstitutionInformation ADD FOREIGN KEY (institutionID) REFERENCES Institution (institutionID);
ALTER TABLE Expenses ADD FOREIGN KEY (institutionID) REFERENCES	Institution	(institutionID);
ALTER TABLE InstitutionType ADD FOREIGN KEY (institutionID) REFERENCES Institution(institutionID);
ALTER TABLE InstitutionDegree ADD FOREIGN KEY (institutionID) REFERENCES	Institution	(institutionID)	;
ALTER TABLE InstitutionDegree ADD FOREIGN KEY (degreeTypeLevel) REFERENCES DegreeDetails(degreeTypeLevel);
ALTER TABLE InstitutionDegree ADD FOREIGN KEY (programmeCode) REFERENCES	ProgrammeDetails(programmeCode);
ALTER TABLE InstitutionUser ADD FOREIGN KEY (institutionID)	REFERENCES	Institution (institutionID)	;
ALTER TABLE InstitutionUser ADD FOREIGN KEY (id) REFERENCES	User (id);
ALTER TABLE UserProfile ADD FOREIGN KEY (id) REFERENCES User (id);
ALTER TABLE InstRepresentative ADD FOREIGN KEY (id) REFERENCES User(id);
ALTER TABLE InstRepresentative ADD FOREIGN KEY (institutionID) REFERENCES Institution (institutionID);
ALTER TABLE InstRepresentative ADD FOREIGN KEY (institutionID) REFERENCES InstitutionUser (institutionID);

-- ----------------------Create Index Queries-----------------------------------

create index Institution_index on Institution (city); 
create index AccredatingAgency_index on AccredatingAgency (accredAgency); 
create index InstitutionInformation_index on InstitutionInformation (governanceStructure); 
create index ProgrammeDetails_index on ProgrammeDetails (programmeDesc); 
create index DegreeDetails_index on DegreeDetails (degreeTypeDesc); 

-- ----------------------  End of Document   -----------------------------------