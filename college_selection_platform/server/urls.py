from django.urls import path
from django.conf.urls import url
from rest_framework_simplejwt import views as jwt_views
from .views import *

urlpatterns = [
    
    path('login/', login_method, name="login"),  
    path('register/', register_method, name="register"), 
    path('logout/', logout_method, name="login"),
    path('all_countries/', get_all_countries_method, name="all_countries"),
    path('is_authenticated/<int:user_id>/', is_authenticated, name="is_authenticated"),
    path('all_admin_data/', get_all_admin_data, name="login"),
    path('edit_user/', edit_user_method, name="edit_user"),
    path('delete_user/<int:user_id>/', delete_user_method, name="delete_user"),
    path('get_user_details/<int:user_id>/', get_user_details_method, name="get_user_details_method"),
    path('all_institutions/', get_all_institutions_method, name="get_all_institutions"),
    path('sort_institutions_by/<str:sort_type>/', sort_institutions_by_method, name="sort_institutions_by"),
    path('institutions/<int:institution_id>/', get_institution_details_method, name="get_institution_details_method")
]
