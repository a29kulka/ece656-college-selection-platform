import React, {useState, useEffect} from "react";
import {withRouter} from 'react-router-dom';
import Header from '../components/Header.js'
import Footer from '../components/Footer.js'
import { Row, Col } from "react-bootstrap";

function Layout(props) {

    return (
        <div style={{ padding: '0px', margin: '0px' }}>
            <Header/>
            <div style={{ margin: '60px 0px 60px 0px'}}>
                {props.children}
            </div>
            <Footer/>
        </div>
    )
}

export default withRouter(Layout)