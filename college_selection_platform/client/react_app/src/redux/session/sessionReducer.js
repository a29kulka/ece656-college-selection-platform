import {
  SIGN_IN,
  SIGN_OUT,
  CLEAR_SESSION,
  USER_CREATED_SUCCESS,
  USER_RESET_PASSWORD_SUCCESS,
  USER_DATA,
  ADMIN_EDIT_USER_DATA
} from "./sessionTypes";

const initialState = {
  isLoggedIn: false,
  userCreatedSuccess: {'is_created': false, 'message_type': ''},
  resetPasswordSuccess: false,
  userData: {},
  adminEditUserData:{}
};

const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN:
      return {
        ...state,
        isLoggedIn: true,
      };
    case SIGN_OUT:
      return {
        ...state,
        isLoggedIn: false,
      };
    case CLEAR_SESSION:
      return {
          isLoggedIn: false,
          userCreatedSuccess: {'is_created': false, 'message_type': ''},
          resetPasswordSuccess:false,
          userData: {},
          adminEditUserData:{}
      };
    case USER_CREATED_SUCCESS:
      return {
        ...state,
        userCreatedSuccess: {'is_created': true, 'message_type': action.payload }
      };
    case USER_RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        resetPasswordSuccess: true
      };
    case USER_DATA:
      return {
        ...state,
        userData: action.payload,
      };
      case ADMIN_EDIT_USER_DATA:
        return {
          ...state,
          adminEditUserData: action.payload,
        };
    default:
      return state;
  }
};

export default sessionReducer;
