import {
  SIGN_IN,
  SIGN_OUT,
  CLEAR_SESSION,
  USER_CREATED_SUCCESS,
  USER_DATA,
  USER_RESET_PASSWORD_SUCCESS,
  ADMIN_EDIT_USER_DATA
} from "./sessionTypes";

export const sign_in = () => {
  return {
    type: SIGN_IN,
  };
};

export const sign_out = () => {
  return {
    type: SIGN_OUT,
  };
};

export const clear_session = () => {
  return {
    type: CLEAR_SESSION,
  };
};

export const user_created_success = (message_type='') => {
  return {
    type: USER_CREATED_SUCCESS,
    payload: message_type
  };
};

export const user_password_reset_success = (message_type='') => {
  return {
    type: USER_RESET_PASSWORD_SUCCESS,
    payload: message_type
  };
};

export const user_data = (user_data = {}) => {
  return {
    type: USER_DATA,
    payload: user_data,
  };
};

export const admin_edit_user_data = (data = {}) => {
  return {
    type: ADMIN_EDIT_USER_DATA,
    payload: data,
  };
};