import React, { useState, useEffect } from "react";
import { withRouter, useHistory, Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import $ from 'jquery'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useDispatch, useSelector } from "react-redux";
import {
    clear_session,
  } from "../redux";
import axiosInstance from "./AxiosInstance";
import logo from "../static/images/logo.png";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import { defaultProfilePictureImageDataUri } from '../constants'
import "../index.css"

const Header = (props) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const curr_user_data = useSelector((state) => state.session.userData);
    const session_is_auth = useSelector((state) => state.session.isLoggedIn);
    const [isAuth, setIsAuth] = useState(false);

    useEffect(() => {
        $('a').on('click', function() {
            $('a').css("color", "white");
            $('a').not(this).removeClass('underline');
            $(this).toggleClass('underline');
        })
    }, [])

    useEffect(() => {
        if(session_is_auth){
            setIsAuth(true);
        }else{
            setIsAuth(false);
        }
    }, [session_is_auth])

    useEffect(() => {
        let link_dict = {
            '/home': 'Dashboard',
            '/login': 'Login',
            '/signup': 'Signup',
            '/institutions': 'Institutions',
            '/': 'Index'
        }
        let links = Array.from(document.getElementsByClassName('nav_links'));
        links.map(item => {
            if(link_dict[history.location.pathname] === item.innerHTML){
                item.style.color = 'black';
                item.classList.add("underline");
            }else{
                item.classList.remove("underline");
            }
        })
    }, [history.location])

    const openChangePasswordModal = () => {
        //pass
    }
    
    const redirectPage = (page) => {
        history.push(page);
    }

    const logout = () => {
        axiosInstance
          .get("logout/")
          .then((response) => {
            if (response.data.ok) {
              dispatch(clear_session());
              localStorage.removeItem("accessToken");
              localStorage.removeItem("refreshToken");
              localStorage.removeItem("userId");
              history.push("/");
            } else {
              console.log("Error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
    };

    return (
        <Navbar className="fixed-top nav-header" collapseOnSelect expand="lg" bg="light">
            <Navbar.Brand style={{ cursor: 'pointer' }} onClick={() => redirectPage(isAuth?'/home':'/')}>
                <Row style={{ margin: '0px', padding: '0px' }}>
                    <Col style={{ margin: '0px', padding: '0px 0px 0px 10px' }} xs={3} sm={3} md={3} lg={3} xl={3}>
                        <img
                            src={logo}
                            height="40"
                            width="40"
                            className="d-inline-block align-top"
                            alt="React Bootstrap logo"
                            style={{ float: 'left', padding: '0px'}}
                        />
                    </Col>
                    <Col style={{ margin: '6px 0px 0px 0px', padding: '0px' }} xs={9} sm={9} md={9} lg={9} xl={9}>
                        <div className="logo_description">College Selection Platform</div>
                    </Col>
                </Row>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
            {isAuth?
            <Nav className="ms-auto">
                <Nav.Link className="nav_links" onClick={() => redirectPage('/home')}>Home</Nav.Link>
                <Nav.Link className="nav_links" onClick={() => redirectPage('/profile')}>Profile</Nav.Link>
                <Nav.Link className="nav_links" onClick={() => redirectPage('/institutions')}>Institutions</Nav.Link>
                <Nav.Link className="nav_links" onClick={() => logout()}>Logout</Nav.Link>
                {/* <DropdownButton
                    alignRight
                    className="nav-btn-profile"
                    title={<Row><Col><img
                        style={{ margin: "0px 0px 0px 0px", width: "40px", height: "40px", borderRadius: "50%"}}
                        src={curr_user_data && curr_user_data.profile_picture ? curr_user_data.profile_picture : defaultProfilePictureImageDataUri}
                        alt="profile_img"/></Col><Col><p style={{ margin: '14px 0px 0px -20px', padding: '0px' }}>{curr_user_data.first_name} {curr_user_data.last_name}</p></Col></Row>}
                        id="dropdown-menu-align-right"
                >
                    <Dropdown.Item><Link style={{color: "black", textDecoration: 'none'}} to='/home'>
                        Dashboard</Link></Dropdown.Item>
                    <Dropdown.Item><Link style={{color: "black", textDecoration: 'none'}} to='/profile'>Your
                        Profile</Link></Dropdown.Item>
                    <Dropdown.Item>
                    <Link style={{color: "black", textDecoration: 'none'}} to='/change_password'>
                    Change Password</Link>
                    </Dropdown.Item>
                    <Dropdown.Divider/>
                    <Dropdown.Item onClick={() => logout()}>Logout</Dropdown.Item>
                </DropdownButton> */}
            </Nav>: 
            <Nav className="ms-auto">
                <Nav.Link className="nav_links" onClick={() => redirectPage('/institutions')}>Institutions</Nav.Link>
                <Nav.Link className="nav_links" onClick={() => redirectPage('/signup')}>Signup</Nav.Link>
                <Nav.Link className="nav_links" style={{ margin: '0px 10px 0px 0px' }} onClick={() => redirectPage('/signin')}>Login</Nav.Link>
                <Nav.Link className="nav_links" onClick={() => redirectPage('/admin')}>Admin</Nav.Link>
            </Nav>
            }
            </Navbar.Collapse>
        </Navbar>
                
    );
}

export default withRouter(Header);
