import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from "react-router-dom";
import '../static/css/custom.css'
import '../index.css'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Alert from 'react-bootstrap/Alert'
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import DataTable from "react-data-table-component";
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import { Multiselect } from 'multiselect-react-dropdown';
import Select from 'react-select'
import { css } from "@emotion/react";
import BarLoader from "react-spinners/BarLoader";
import {
    FaShoppingCart,
    FaFileAlt,
    FaKey,
    FaUserCircle,
    FaSignOutAlt,
    FaInfoCircle,
    FaUser,
    FaUserShield,
    FaPlus,
    FaMinus,
    FaSearch
  } from "react-icons/fa";
import axiosInstance from "../components/AxiosInstance";
import { useDispatch, useSelector } from "react-redux"; 
import {
    clear_session,
  } from "../redux";
import InputGroup from "react-bootstrap/InputGroup";
import { FormControl } from "react-bootstrap";

const Institution = (props) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const curr_user_data = useSelector((state) => state.session.userData);
    const session_is_auth = useSelector((state) => state.session.isLoggedIn);
    const [isAuth, setIsAuth] = useState(false);
    const [filterValue, setFilterValue] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [filterOptions, setFilterOptions] = useState([{'label': 'Rankings (High to low)', 'value': 'Rankings'},{'label': 'Reviews  (High to low)', 'value': 'Reviews'}, {'label': 'Ratings  (High to low)', 'value': 'Ratings'}])
    const [allInstitutions, setAllInstitutions] = useState([]);

    const [loading, setLoading] = useState(false);
    const [color, setColor] = useState("#0C6EFD");

    
    const override = css`
        display: block;
        margin: 16% 46%;
        border-color: #0C6EFD;
        z-index: 100;
        position: absolute;
    `;

    const getAllInstitutions = () => {
        setLoading(true);
        axiosInstance
          .get("all_institutions/")
          .then((response) => {
            if (response.data.ok) {
                setLoading(false);
                let all_institutions = JSON.parse(response.data.all_institutions);
                setAllInstitutions(all_institutions)
            } else {
            setLoading(false);
              console.log("Error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
    };

    const sortBy = (type) => {
        setLoading(true);
        axiosInstance
          .get("sort_institutions_by/"+type+"/")
          .then((response) => {
            if (response.data.ok) {
                setLoading(false);
                let all_institutions = JSON.parse(response.data.all_institutions);
                setAllInstitutions(all_institutions)
            } else {
            setLoading(false);
              console.log("Error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
    };

    useEffect(() => {
        getAllInstitutions();
        if(session_is_auth){
            setIsAuth(true);
        }else{
            setIsAuth(false);
        }
    }, [session_is_auth])

    const redirectPage = (page) => {
        history.push(page);
    }

    const handleChange = (e, type, component) => {

        if(component === 'search'){
            e.preventDefault();
            let val = e.target.value;
            setSearchValue(val);
            // pass
        }else{
            let val = e.value;
            setFilterValue(val);
            sortBy(val);
        }
    }
    return (
        <div>
            <div style={{ margin: '0px', padding: '30px 0px 0px 0px'}} className="row">
                <div style={{ marginBottom: '10px' }} className="col-sm-12 col-md-5 col-lg-3 col-xl-3">
                    <InputGroup>
                        <FormControl
                            name="search"
                            value={searchValue}
                            onChange={(e) => handleChange(e, 'text', 'search')} 
                            type="text" 
                            className="form-control" 
                            placeholder="Search" 
                            aria-describedby="search-btn"
                        />
                        <Button variant="outline-secondary" id="search-btn">
                            <FaSearch onClick={(e) => handleChange(e, 'text', 'search')} />
                        </Button>
                    </InputGroup>
                </div>
                <div style={{ marginBottom: '10px' }} className="col-sm-12 col-md-5 offset-md-2 col-lg-3 offset-lg-6 col-xl-3 offset-xl-6">
                    <Select
                        onChange={value => handleChange(value, 'dropdown', 'filter')}
                        className="filter"
                        placeholder={'Sort by'}
                        classNamePrefix="select_filter"
                        isSearchable={true}
                        name="filter"
                        options={filterOptions}
                    />
                </div>
            </div>
            <BarLoader color={color} loading={loading} css={override} size={250} />
            {!loading?<Row style={{ padding: '0px', margin: '10px 0px 60px 0px' }}>
                {allInstitutions.map((item, idx) => {
                    return (
                    <Col key={idx} style={{ padding: '5px 15px', margin: '0px 0px 0px 0px' }} xs={12} sm={6} md={4} lg={4} xl={3}>
                        <Card className="shadow">
                            <Card.Body>
                                <Card.Title style={{ color: "#0C6EFD" }}>{item.institutionName}</Card.Title>
                                <Card.Subtitle style={{ fontSize: '0.8rem' }}className="mb-2 text-muted">
                                    {item.city}, {item.state} - {item.zipCode}
                                </Card.Subtitle>
                                <Card.Text className="scroll_bar_card">
                                    <p style={{ margin: '0px'}}>Ranking - {item.ranking}</p>
                                    <p style={{ margin: '0px 0px 10px 0px'}}>Ratings - {item.rating} (Out of 10)</p>
                                    <p style={{ fontSize: '12px', margin: '0px'}}>Based on {item.numreviews}</p>
                                </Card.Text>
                                <Card.Link style={{ textDecoration: 'none', cursor: 'pointer', color: '##0C6EFD' }} onClick={() => redirectPage("/institutions/"+item.institutionID)}>Details</Card.Link>
                            </Card.Body>
                        </Card>
                    </Col>)
                    })
            }
            </Row>:''}
        </div>
    );
    }

export default withRouter(Institution);
