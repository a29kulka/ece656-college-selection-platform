import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from "react-router-dom";
import '../static/css/custom.css'
import '../index.css'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Alert from 'react-bootstrap/Alert'
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import DataTable from "react-data-table-component";
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import { Multiselect } from 'multiselect-react-dropdown';
import {
    FaShoppingCart,
    FaFileAlt,
    FaKey,
    FaUserCircle,
    FaSignOutAlt,
    FaInfoCircle,
    FaUser,
    FaUserShield,
    FaPlus,
    FaMinus
  } from "react-icons/fa";
import axiosInstance from "../components/AxiosInstance";
import { useDispatch, useSelector } from "react-redux"; 
import {
    clear_session,
  } from "../redux";

const Home = (props) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const curr_user_data = useSelector((state) => state.session.userData);
    const session_is_auth = useSelector((state) => state.session.isLoggedIn);
    const [isAuth, setIsAuth] = useState(false);

    const logout = () => {
        axiosInstance
          .get("logout/")
          .then((response) => {
            if (response.data.ok) {
              dispatch(clear_session());
              localStorage.removeItem("accessToken");
              localStorage.removeItem("refreshToken");
              localStorage.removeItem("userId");
              history.push("/");
            } else {
              console.log("Error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
    };

    useEffect(() => {
        if(session_is_auth){
            setIsAuth(true);
        }else{
            setIsAuth(false);
        }
    }, [session_is_auth])

    const redirectPage = (page) => {
        history.push(page);
    }

    return (
            <Row style={{ padding: '0px', margin: '12% 0% 0% 0%' }}>
                <Col>
                Home <br/> <Button style={{ marginTop: '20px' }} onClick={logout}>Logout</Button>
                </Col>
            </Row>
    );
    }

export default withRouter(Home);
