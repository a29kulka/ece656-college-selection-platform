import React, { useState, useEffect, useRef } from "react";
import { withRouter, useHistory } from "react-router-dom";
import '../static/css/custom.css'
import '../index.css'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Alert from 'react-bootstrap/Alert'
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import DataTable from "react-data-table-component";
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import { Multiselect } from 'multiselect-react-dropdown';
import { css } from "@emotion/react";
import BarLoader from "react-spinners/BarLoader";
import Dialog from "react-bootstrap-dialog";
import {
    FaShoppingCart,
    FaFileAlt,
    FaKey,
    FaUserCircle,
    FaSignOutAlt,
    FaInfoCircle,
    FaUser,
    FaUserShield,
    FaPlus,
    FaMinus,
    FaTrash,
    FaEdit
  } from "react-icons/fa";
import axiosInstance from "../components/AxiosInstance";
import { useDispatch, useSelector } from "react-redux"; 

import {
    clear_session, admin_edit_user_data
  } from "../redux";

const AdminDashboard = (props) => {

  let CustomDialog = useRef(null);

    const history = useHistory();
    const dispatch = useDispatch();
    const curr_user_data = useSelector((state) => state.session.userData);
    const session_is_auth = useSelector((state) => state.session.isLoggedIn);
    const [isAuth, setIsAuth] = useState(false);
    const [allUsersData, setAllUsersData] = useState([]);
    const [allInstitutionsData, setAllInstitutionsData] = useState([]);

    const [loading, setLoading] = useState(false);
    const [color, setColor] = useState("#0C6EFD");


    const override = css`
        display: block;
        margin: 16% 46%;
        border-color: #0C6EFD;
        z-index: 100;
        position: absolute;
    `;

    const user_columns = [{
      name: "Id",
      selector: "id",
      sortable: true,
      center:true,
      width: '120px',
      cell: (row) => <span>{row.id?row.id:'-'}</span>
    },{
        name: "First Name",
        selector: "firstName",
        sortable: true,
        center:true,
        width: '120px',
        cell: (row) => <span>{row.firstName?row.firstName:'-'}</span>
    },
    {
        name: "Last Name",
        selector: "lastName",
        sortable: true,
        center:true,
        width: '180px',
        cell: (row) => <span>{row.lastName?row.lastName:'-'}</span>
    },
    {
      name: "Username",
      selector: "username",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.username?row.username:'-'}</span>
  },
    {
        name: "Email",
        selector: "email",
        sortable: true,
        center:true,
        width: '180px',
        cell: (row) => <span>{row.email?row.email:'-'}</span>
    },
    {
        name: "Role",
        selector: "role",
        sortable: true,
        center:true,
        width: '180px',
        cell: (row) => <span>{row.role?row.role:'-'}</span>
    },
    {
      name: "Created On",
      selector: "createdOn",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.createdOn?row.createdOn:'-'}</span>
  },
  {
    name: "Updated On",
    selector: "updatedOn",
    sortable: true,
    center:true,
    width: '180px',
    cell: (row) => <span>{row.updatedOn?row.updatedOn:'-'}</span>
  },
  {
    name: "City",
    selector: "city",
    sortable: true,
    center:true,
    width: '180px',
    cell: (row) => <span>{row.city?row.city:'-'}</span>
  },{
    name: "State",
    selector: "state",
    sortable: true,
    center:true,
    width: '180px',
    cell: (row) => <span>{row.state?row.state:'-'}</span>
  },{
    name: "Country",
    selector: "country",
    sortable: true,
    center:true,
    width: '180px',
    cell: (row) => <span>{row.country?row.country:'-'}</span>
  },{
    name: "Zip code",
    selector: "zipCode",
    sortable: true,
    center:true,
    width: '180px',
    cell: (row) => <span>{row.zipCode?row.zipCode:'-'}</span>
  },,{
    name: "Actions",
    selector: "actions",
    sortable: true,
    center:true,
    cell: (row) => 
    <Row style={{ padding: '0px', margin: '0px' }}>
        <Col xs={6} sm={6} md={6} lg={6} xl={6} style={{ padding: '0px', margin: '0px' }}>
            <ActionComponent type={'edit'} row={row} onClick={() => handleRowClick(row)}/>
        </Col>
        <Col xs={6} sm={6} md={6} lg={6} xl={6} style={{ padding: '0px', margin: '0px' }}>
            <ActionComponent type={'delete'} row={row} onClick={() => deleteUserRef(row.id)}/>
        </Col>
      </Row>
  }];

    const institutions_columns = [{
      name: "Institution Id",
      selector: "institutionID",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.institutionID?row.institutionID:'-'}</span>
    },{
      name: "Institution Name",
      selector: "institutionName",
      sortable: true,
      center:true,
      width: '240px',
      cell: (row) => <span>{row.institutionName?row.institutionName:'-'}</span>
    },{
      name: "Ranking",
      selector: "ranking",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.ranking?row.ranking:'-'}</span>
    },{
      name: "Ratings (Out of 5)",
      selector: "rating",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.rating?row.rating/2:'-'}</span>
    },{
      name: "Reviews",
      selector: "numreviews",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.numreviews?row.numreviews:'-'}</span>
    },{
      name: "City",
      selector: "city",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.city?row.city:'-'}</span>
    },{
      name: "State",
      selector: "state",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.state?row.state:'-'}</span>
    },{
      name: "Zip code",
      selector: "zipCode",
      sortable: true,
      center:true,
      width: '180px',
      cell: (row) => <span>{row.zipCode?row.zipCode:'-'}</span>
    }]

    useEffect(() => {
        getAllAdminData();
        if(session_is_auth){
            setIsAuth(true);
        }else{
            setIsAuth(false);
        }
    }, [session_is_auth])

    const redirectPage = (page) => {
        history.push(page);
    }

    const ActionComponent = ({  row, onClick, type  }) => {
      if(type === 'delete'){
        const clickHandler = () => onClick(row);
        return <FaTrash className="delete_icon" onClick={clickHandler}/>
        }else if(type === 'edit'){
        const clickHandler = () => onClick(row);
        return <FaEdit className="edit_icon" onClick={clickHandler}/>
      }
  };


  const deleteUserRef = (id) => {
        
    CustomDialog.show({
        body:"Are you sure you want to delete this user ?",
        actions: [
        Dialog.DefaultAction(
            "Delete",
            () => {deleteUser(id)},
            "btn-danger"
        ),
        Dialog.Action(
            "Close",
            () => {
               if(CustomDialog){
                   CustomDialog.hide()
               }
            },
            "btn-primary"
        ),
        ],
    });
};

  const deleteUser = (id) => {
    axiosInstance
          .get("delete_user/"+id+"/")
          .then((response) => {
            if (response.data.ok) {
              getAllAdminData();
            } else {
              console.log("Error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
  }


    const getAllAdminData = () => {
      setLoading(true);
        axiosInstance
          .get("all_admin_data/")
          .then((response) => {
            if (response.data.ok) {
              setLoading(false);
              setAllUsersData(JSON.parse(response.data.all_users));
              setAllInstitutionsData(JSON.parse(response.data.all_institutions));
            } else {
              setLoading(false);
              console.log("Error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
    };


    const handleRowClick = (data) => { 
      dispatch(admin_edit_user_data(data));
      history.push('/admin_dashboard/users/'+data.id);
    }

    return (
            <Row style={{ padding: '0px', margin: '8% 0% 0% 0%' }}>
              <div style={{ display: "none" }}>
                  <Dialog
                      ref={(el) => {
                          CustomDialog = el;
                      }}
                  />
              </div>
                {!loading?<Col xs={12} sm={12} md={12} lg={12} xl={12}>
                  <p style={{ fontSize: "20px", fontWeight: 'bold' }}>Users</p>
                    <DataTable
                        columns={user_columns}
                        data={allUsersData}
                        pagination
                        paginationRowsPerPageOptions={[10, 15, 20, 25, 30]}
                        paginationPerPage={10}
                    />
                </Col>:""}
                <BarLoader color={color} loading={loading} css={override} size={250} />
                {!loading?<Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <p style={{ fontSize: "20px", fontWeight: 'bold' }}>Instituions</p>
                    <DataTable
                        columns={institutions_columns}
                        data={allInstitutionsData}
                        pagination
                        paginationRowsPerPageOptions={[10, 15, 20, 25, 30]}
                        paginationPerPage={10}
                    />
                </Col>:""}
            </Row>
    );
    }

export default withRouter(AdminDashboard);
