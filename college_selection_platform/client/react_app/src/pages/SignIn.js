import React, { useState, useEffect } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import Modal from "react-bootstrap/Modal";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { Orientation } from "../components/Orientation";
import {
  FaEye,
  FaEyeSlash,
  FaEnvelopeOpen,
  FaUserShield,
  FaCheckCircle,
} from "react-icons/fa";
import { API_URL, SSO_PASS } from "../constants/";
import {
  sign_in,
  user_data,
  user_created_success,
} from "../redux";
import axios from "axios";
import { Link, useHistory, withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import CSRFToken from "../components/Csrf";
import "../index.css";
import axiosInstance from "../components/AxiosInstance";

const SignIn = (props) => {
  // const [windowDimensions, setWindowDimensions] = useState({
  //   height: window.innerHeight,
  //   width: window.innerWidth,
  // });
  const history = useHistory();
  const dispatch = useDispatch();
  const store_user_created_success = useSelector(
    (state) => state.session.user_created_success
  );
  const [userCreatedMsg, setUserCreatedMsg] = useState("");
  const [resetPasswordMsg, setResetPasswordMsg] = useState("");
  const [orientation, setOrientation] = useState("default");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [resetPasswordSuccessMsg, setResetPasswordSuccessMsg] = useState("");
  const [password, setPassword] = useState("");
  const [isValidLogin, setIsValidLogin] = useState(true);
  const [isIncorrectPassword, setIsIncorrectPassword] = useState("");
  const [isIncorrectEmail, setIsIncorrectEmail] = useState(false);
  const [incorrectEmailMsg, setIncorrectEmailMsg] = useState("");
  const [isValidResetEmail, setIsValidResetEmail] = useState(false);
  const [isValidResetPassword, setIsValidResetPassword] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showResetPassword, setShowResetPassword] = useState(false);
  const [showResetPasswordConfirm, setShowResetPasswordConfirm] = useState(
    false
  );
  const [isAdmin, setIsAdmin] = useState(false);
  const [resetPasswordModal, setResetPasswordModalShow] = useState(false);
  const [tabKey, setTabKey] = useState("email");
  const [resetPasswordFormData, setResetPasswordFormData] = useState({
    resetPassword: "",
    resetPasswordConfirm: "",
  });
  const [resetEmailFormData, setResetEmailFormData] = useState({
    resetEmail: "",
  });
  const [formErrors, setFormErrors] = useState({
    resetEmail: "",
    resetPassword: "",
    resetPasswordConfirm: "",
    email:"",
    username:""
  });
  const [resetPasswordEmailModal, setResetPasswordEmailModal] = useState(false);
  const [resetPasswordModalMethodError, setResetPasswordModalMethodError] = useState(false);

  const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );

  const passwordRegex = RegExp(
    /^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*[^\w\s])\S{6,}$/
  );

  let session_user_created_success = useSelector(
    (state) => state.session.userCreatedSuccess
  );

  let session_reset_password_success = useSelector(
    (state) => state.session.resetPasswordSuccess
  );

  useEffect(() => {
    if (session_user_created_success.message_type === 'sso_login') {
      setUserCreatedMsg("Your account has been created successfully with Google. Please reset your password");
      setTimeout(function () {
        setUserCreatedMsg("");
      }, 10000);
    }else if(session_user_created_success.message_type === 'login'){
      setUserCreatedMsg("Your account has been created successfully.");
      setTimeout(function () {
        setUserCreatedMsg("");
      }, 10000);
    }
  }, [session_user_created_success]);

  useEffect(() => {
    if (session_reset_password_success) {
      setResetPasswordMsg("Password has been reset successfully, Login with your new password.");
      setTimeout(function () {
        setResetPasswordMsg("");
      }, 10000);
    }
  }, [session_reset_password_success])

  useEffect(() => {
    if(history.location.pathname == '/admin'){
      setIsAdmin(true);
    }
  }, [history])

//   const blinkElement = () => {
//     var ele = document.getElementById("forgot_password");
//     ele.className += " blink_me";
//   };

  const redirectPage = (page) => {
    history.push(page);
  }

  const passwordVisibilityToggle = (id, reset, type) => {
    if (reset) {
      if (type === "new") {
        let reset_password = !showResetPassword;
        setShowResetPassword(reset_password);
      } else {
        let reset_password_confirm = !showResetPasswordConfirm;
        setShowResetPasswordConfirm(reset_password_confirm);
      }
    } else {
      let login_password = !showPassword;
      setShowPassword(login_password);
    }
  };

  const handleSubmit = (e, type, data) => {
    
    let post_data = {};
    if (type === "sso") {
      post_data = data;
    } else {
      e.preventDefault();
      post_data = { username: username, password: password, type: type };
    }
    post_data['is_admin'] = isAdmin;
    post_data['role'] = 'Admin'
    axiosInstance
      .post("login/", post_data)
      .then((res) => {
        if (res.data.ok) {
          dispatch(sign_in());
          setIsValidLogin(true);
          dispatch(user_data(res.data.user));
          localStorage.setItem('userId', res.data.user.id);
          if(res.data.user.role === 'Student'){
            history.push("/");
          }else{
            history.push("/admin_dashboard");
          }
        } else {
          let err_msg = res.data
            ? res.data.error
            : "Some of the fields are incorrect.";
          setIsValidLogin(false);
          setResetPasswordMsg("");
          setErrorMessage(err_msg);
          setUserCreatedMsg("");
          setTimeout(function () {
            setErrorMessage("");
          }, 10000);
        }
      })
      .catch((err) => {
        let err_msg = err
            ? err.message
            : "Some of the fields are incorrect.";
          setIsValidLogin(false);
          setErrorMessage(err_msg);
          setResetPasswordMsg("");
          setUserCreatedMsg("");
          setTimeout(function () {
            setErrorMessage("");
          }, 10000);
        console.log(err);
      });
  };

  const formValid = (formErrors, formData, type) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach((val) => {
      val.length > 0 && (valid = false);
    });
    if (Object.values(formData).length <= 0) {
      valid = false;
    }
    Object.values(formData).forEach((val) => {
      if (!val || val === "") {
        valid = false;
      }
    });

    return valid;
  };

  const resetPassword = () => {
    if (formValid(formErrors, resetPasswordFormData, "password")) {
      setIsValidResetPassword(true);
      closeResetPasswordModal();
      let post_data = {};
      post_data["password"] = resetPasswordFormData.resetPassword;
      post_data["email"] = resetEmailFormData.resetEmail;
      axiosInstance
        .post("forgot_password/", post_data)
        .then((res) => {
          if (res.data.ok) {
            setResetPasswordSuccessMsg("Password has been reset successfully, Login with your new password.")
            setTimeout(function(){
              setResetPasswordSuccessMsg("")
            }, 10000)
          } else {
            console.log("error");
          }
        })
        .catch((err) => {
          console.log("Error");
        });
    } else {
      setIsValidResetPassword(false);
      setFormErrors({ ...formErrors, resetPassword: "" });
      setFormErrors({ ...formErrors, resetPasswordConfirm: "" });
      setIsIncorrectPassword(true);
      console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
    }
  };

  const handleChange = (e) => {

    e.preventDefault();
    const { name, value } = e.target;
    let password = resetPasswordFormData.resetPassword;
    if (tabKey == "password") {
      let curr_reset_password = document.getElementById("reset_password").value;
      let curr_reset_confirm_password = document.getElementById(
        "reset_password_confirm"
      ).value;
      if (
        curr_reset_password !== curr_reset_confirm_password &&
        curr_reset_password.length > 0 &&
        curr_reset_confirm_password.length > 0
      ) {
        formErrors.resetPasswordConfirm =
          "password and confirm password should be the same";
      } else {
        formErrors.resetPasswordConfirm = "";
      }
    }
    validate(name, value, password);
  };

  const validate = (name, value, password) => {
    setIncorrectEmailMsg("");
    switch (name) {
      case "email":
        setEmail(value);
          if (emailRegex.test(value)) {
            setFormErrors({ ...formErrors, email: "" });
          } else {
            if (value.length > 0) {
              setFormErrors({ ...formErrors, email: "Invalid email address" });
            } else {
              setFormErrors({ ...formErrors, email: "" });
            }
          }
          break;
      case "username":
        setUsername(value);
        if(value.length <= 0){
          setFormErrors({ ...formErrors, username: "Invalid email address" });
        }else if(value.length > 10){
          setFormErrors({ ...formErrors, username: "username must have less than 10 characters." });
        }else{
          setFormErrors({ ...formErrors, username: "" });
        }
      case "resetEmail":
        setResetEmailFormData({ ...resetEmailFormData, resetEmail: value });
        if (emailRegex.test(value)) {
          setFormErrors({ ...formErrors, resetEmail: "" });
        } else if (value.length === 0) {
          setFormErrors({
            ...formErrors,
            resetEmail: "email field is required",
          });
        } else {
          if (value.length > 0) {
            setFormErrors({
              ...formErrors,
              resetEmail: "invalid email address",
            });
          } else {
            setIsIncorrectEmail(false);
            setFormErrors({ ...formErrors, resetEmail: "" });
          }
        }
        break;
      case "resetPassword":
        setIsIncorrectPassword(false);
        setResetPasswordFormData({
          ...resetPasswordFormData,
          resetPassword: value,
        });
        if (passwordRegex.test(value)) {
          setFormErrors({ ...formErrors, resetPassword: "" });
        } else if (value.length == 0) {
          setFormErrors({
            ...formErrors,
            resetPassword: "password field is required",
          });
        } else {
          if (value.length > 0) {
            setFormErrors({
              ...formErrors,
              resetPassword:
                "password must contain minimum 6, at least one uppercase letter, one lowercase letter, one number and one special character",
            });
          } else {
            setFormErrors({ ...formErrors, resetPassword: "" });
          }
        }
        break;
      case "resetPasswordConfirm":
        setResetPasswordFormData({
          ...resetPasswordFormData,
          resetPasswordConfirm: value,
        });
        if (value.length > 0 && value !== password) {
          setFormErrors({
            ...formErrors,
            resetPasswordConfirm:
              "password and confirm password should be the same",
          });
        } else if (value.length == 0) {
          setFormErrors({
            ...formErrors,
            resetPasswordConfirm: "confirm password field is required",
          });
        } else {
          setIsIncorrectPassword(false);
          setFormErrors({ ...formErrors, resetPasswordConfirm: "" });
        }
        break;
      default:
        break;
    }
  };


  const sendResetPasswordEmail = () => {
    let post_data = {};
    setIsDisabled(true);
    if (formValid(formErrors, resetEmailFormData, "email")) {
      post_data["email"] = resetEmailFormData.resetEmail;
      axios
        .post(API_URL+'password_reset_email/', post_data)
        .then((res) => {
          setIsDisabled(false);
          closeResetPasswordEmailModal();
          if (res.data.ok) {
            setResetPasswordModalMethodError(false);
            setResetPasswordSuccessMsg(res.data.message);
            setTimeout(function () {
              setResetPasswordSuccessMsg("");
            }, 10000);
          } else {
            setResetPasswordModalMethodError(true);
            setResetPasswordSuccessMsg(res.data.error);
            setTimeout(function () {
              setResetPasswordSuccessMsg("");
            }, 10000);
          }
        })
        .catch((err) => {
            setResetPasswordModalMethodError(true);
            setIsDisabled(false);
            console.log(err);
        });
    } else {
      //pass
    }
  }

  const openResetPasswordModal = () => {
    setIsValidResetEmail(false);
    setTabKey("email");
    setResetPasswordModalShow(true);
    setFormErrors({
      ...formErrors,
      resetEmail: "",
    });
    setIncorrectEmailMsg("");
    setResetPasswordFormData({
      resetPassword: "",
      resetPasswordConfirm: "",
    });
    setResetEmailFormData({ resetEmail: "" });
  };

  const closeResetPasswordModal = () => {
    setResetPasswordModalShow(false);
    if(props.admin){
      history.push("/admin");
    }else{
      history.push("/login");
    }
  };

  const openResetPasswordEmailModal = () => {
    setIsValidResetEmail(false);
    setResetPasswordEmailModal(true);
    setResetEmailFormData({ resetEmail: "" });
    setFormErrors({
      ...formErrors,
      resetEmail: "",
    });
    setIncorrectEmailMsg("");
  }

  const closeResetPasswordEmailModal = () => {
    setResetPasswordEmailModal(false);
  }

  const validateEmail = () => {
    
    let post_data = {};
    if (formValid(formErrors, resetEmailFormData, "email")) {
      post_data["email"] = resetEmailFormData.resetEmail;
      axios
        .post(API_URL+"validate_email/", post_data)
        .then((res) => {
          if (res.data.ok) {
            setIsValidResetEmail(true);
            setTabKey("password");
          } else {
            setIsValidResetEmail(false);
            setFormErrors({ ...formErrors, resetEmail: "" });
            setIsIncorrectEmail(true);
            setIncorrectEmailMsg("No such user exists");
          }
        })
        .catch((err) => {
          setIsValidResetEmail(false);
          setFormErrors({ ...formErrors, resetEmail: "" });
          setIsIncorrectEmail(true);
          setIncorrectEmailMsg("No such user exists");
        });
      console.log("VALID");
    } else {
      setIsValidResetEmail(false);
      setFormErrors({ ...formErrors, resetEmail: "" });
      setIsIncorrectEmail(true);
      setIncorrectEmailMsg("Some of the fields are empty or incorrect");
      console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
    }
  };

  return (
    <div>
      <Row>
        <Col style={{ margin: '10px 0px 10px 0px' }} lg="12" md="12" sm="12" xs="12">
          <span style={{ fontWeight: 'bold', fontStyle: 'normal', fontFamily: 'Roboto', fontSize: '25px', lineHeight: '40px' }} >{!isAdmin?'LOG IN':'ADMIN LOG IN'}</span>
        </Col>
        <Col g="12" md="12" sm="12" xs="12">
          <Form style={{ padding: '20px 100px' }} onSubmit={(e) => handleSubmit(e, "login", "")}>
              <CSRFToken />
              <Form.Group id="username">
                  <Form.Label className="float-start">Username</Form.Label>
                  <Form.Control
                      name="username"
                      type="username"
                      placeholder="Enter Username"
                      value={username}
                      onChange={(e) => handleChange(e)}
                      required
                      >
                  </Form.Control>
                  <Row style={{ padding: "0px", margin: "0px" }}>
                      <Col style={{ padding: "0px" }}>
                      {formErrors.username && formErrors.username.length > 0 && (
                      <span className="float-start error_message">
                          {formErrors.username}
                      </span>
                      )}
                      </Col>
                  </Row>
                </Form.Group>
              <Form.Group id="password">
                <Form.Label className="float-start">Password</Form.Label>
                <Form.Control
                  autoComplete="off"
                  name="password"
                  required
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                >
                </Form.Control>
            </Form.Group>
            <Row style={{ margin: '25px 0px 0px 0px',  padding: "0px 10px"  }}>
                <Col style={{ margin: '0px', padding: '0px' }}>
                    <Button
                        size="lg"
                        className="btn-block btn-primary"
                        type="submit"
                    >
                    <span style={{ fontSize: '16px', fontWeight: 'bolder' }}>LOG IN</span>
                    </Button>
                </Col>
            </Row>
            {!isValidLogin ? (
              <Row style={{ margin: "0px", padding: "0px" }}>
                <Col>
                  <p
                    style={{ fontSize: "0.9rem", marginTop: '5px' }}
                    className="text-center sign_in_error_message"
                  >
                    {errorMessage}
                  </p>
                </Col>
              </Row>
            ) : (
              ""
            )}
            {session_user_created_success ? (
              <Row style={{ margin: "15px 0px 0px 0px", padding: "0px" }}>
              <Col>
                <p
                  style={{ fontSize: "1rem" }}
                  className="text-center user_created_message_login"
                >
                  {userCreatedMsg}
                </p>
              </Col>
            </Row>) : (
              ""
            )}
            {resetPasswordMsg ? (
              <Row style={{ margin: "0px", padding: "0px" }}>
                <Col>
                  <p
                    style={{ fontSize: "0.8rem" }}
                    className="text-center reset_password_success_msg"
                  >
                    {resetPasswordMsg}
                  </p>
                </Col>
              </Row>
            ) : (
              ""
            )}
            {!isAdmin?<Row style={{ margin: "0px", padding: "0px" }}>
              <Col
                xs={12}
                sm={12}
                md={12}
                lg={12}
                xl={12}
              >
                <div onClick={() => redirectPage('/signup')} style={{  textAlign: 'left', margin: '10px 0px 0px 0px' }}><div>Don't have an account ? <b style={{ fontWeight: 'bolder', cursor: 'pointer'}}>Sign up</b></div> </div>                  
              </Col>
            </Row>:''}
          </Form>
        </Col>
      </Row>
    </div>
  );
}

export default withRouter(SignIn);
