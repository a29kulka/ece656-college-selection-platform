import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from "react-router-dom";
import '../static/css/custom.css'
import '../index.css'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Alert from 'react-bootstrap/Alert'
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import DataTable from "react-data-table-component";
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import Nav from 'react-bootstrap/Nav'
import Select from 'react-select'
import {
    FaShoppingCart,
    FaFileAlt,
    FaKey,
    FaUserCircle,
    FaSignOutAlt,
    FaInfoCircle,
    FaUser,
    FaUserShield,
    FaPlus,
    FaMinus,
    FaPray
  } from "react-icons/fa";
import Carousel from 'react-bootstrap/Carousel'
import carousel1 from '../static/images/carousel_1.jpg'
import carousel2 from '../static/images/carousel_2.jpg'
import carousel3 from '../static/images/carousel_3.jpg'
import carousel4 from '../static/images/carousel_4.jpeg'
import axiosInstance from "../components/AxiosInstance";
import { useDispatch, useSelector } from "react-redux"; 
import Ratings from 'react-ratings-declarative';
import {
    clear_session,
    user_data
  } from "../redux";
import { API_URL } from "../constants";
import CSRFToken from "../components/Csrf";
import { map } from "jquery";

const Home = (props) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const curr_user_data = useSelector((state) => state.session.userData);
    const session_is_auth = useSelector((state) => state.session.isLoggedIn);
    const [isAuth, setIsAuth] = useState(false);
    const [currInstitutionId, setCurrInstitutionId] = useState('');
    const [currInstitutionData, setCurrInstitutionData] = useState({});
    const [degreeProgramDict, setDegreeProgramDict] = useState({});
    const [tab, setTab] = useState(1);
    const [degreeOptions, setDegreeOptions] = useState([]);
    const [isFormSubmitError, setIsFormSubmitError] = useState(false);
    const [programList, setProgramList] = useState([]);

    const [formData, setFormData] = useState({
        'degree': '',
        'rating': 0,
        'reviews': 0,
        'ranking': '',
        'accredBy': '',
        'institutionName': ''
    })
    const [formErrors, setFormErrors] = useState({
        'degree': '',
        'rating': '',
        'reviews': '',
        'ranking': '',
        'accredBy': '',
        'institutionName': ''
    })
    
    const fees_columns = [
        {
          name: "Total Fees",
          selector: "totalFee",
          sortable: true,
          center:true,
          width: '120px',
          cell: (row) => <span>{row.totalFee?'$' + row.totalFee:'-'}</span>
      },
      {
          name: "Tuition Fee (In State)",
          selector: "tuitionFeeInState",
          sortable: true,
          center:true,
          width: '320px',
          cell: (row) => <span>{row.tuitionFeeInState?'$' + row.tuitionFeeInState:'-'}</span>
      },
      {
          name: "Tuition Fee (Out State)",
          selector: "tuitionFeeOutState",
          sortable: true,
          center:true,
          width: '180px',
          cell: (row) => <span>{row.tuitionFeeOutState?'$' + row.tuitionFeeOutState:'-'}</span>
      },
      {
          name: "Book Supplies",
          selector: "bookSupplies",
          sortable: true,
          center:true,
          cell: (row) => <span>{row.bookSupplies?'$' + row.bookSupplies:'-'}</span>
      },
      {
          name: "Housing",
          selector: "housing",
          sortable: true,
          center:true,
          cell: (row) => <span>{row.housing?'$' + row.housing:'-'}</span>
      },
      {
          name: "Miscallaneous",
          selector: "miscellaneous",
          sortable: true,
          center:true,
          width: '180px',
          cell: (row) => <span>{row.miscellaneous?'$' + row.miscellaneous:'-'}</span>
      }
    ]

    const details_columns = [
        {
          name: "Main Campus",
          selector: "mainCampus",
          sortable: true,
          center:true,
          width: '150px',
          cell: (row) => <span>{row.mainCampus?row.mainCampus:'-'}</span>
      },
      {
          name: "Number of Branches",
          selector: "numBranch",
          sortable: true,
          center:true,
          width: '180px',
          cell: (row) => <span>{row.numBranch?row.numBranch:'-'}</span>
      },
      {
          name: "Governance Structure",
          selector: "governanceStructure",
          sortable: true,
          center:true,
          width: '180px',
          cell: (row) => <span>{row.governanceStructure?row.governanceStructure:'-'}</span>
      },
      {
          name: "Affiliation",
          selector: "affiliation",
          sortable: true,
          center:true,
          cell: (row) => <span>{row.affiliation?row.affiliation:'-'}</span>
      },
      {
          name: "Admission Rate (%)",
          selector: "admissionRate",
          sortable: true,
          width: '180px',
          center:true,
          cell: (row) => <span>{row.admissionRate?row.admissionRate:'-'}</span>
      },
      {
          name: "Total Admissions",
          selector: "totalAdmissions",
          sortable: true,
          center:true,
          width: '180px',
          cell: (row) => <span>{row.totalAdmissions?row.totalAdmissions:'-'}</span>
      },
      {
        name: "Part time admissions (%)",
        selector: "pctPartTimeAdmissions",
        sortable: true,
        center:true,
        width: '220px',
        cell: (row) => <span>{row.pctPartTimeAdmissions?row.pctPartTimeAdmissions:'-'}</span>
    },
    {
        name: "Course Completion Rate",
        selector: "completionRate",
        sortable: true,
        center:true,
        width: '180px',
        cell: (row) => <span>{row.completionRate?row.completionRate:'-'}</span>
    },
    {
        name: "On Campus Housing",
        selector: "onCampusHousing",
        sortable: true,
        center:true,
        width: '180px',
        cell: (row) => <span>{row.onCampusHousing?row.onCampusHousing:'-'}</span>
    },
    {
        name: "Employee Satisfaction",
        selector: "employeeSatisfaction",
        sortable: true,
        center:true,
        width: '180px',
        cell: (row) => <span>{row.employeeSatisfaction?row.employeeSatisfaction:'-'}</span>
    },
    {
        name: "Transport Facility",
        selector: "transportFacility",
        sortable: true,
        center:true,
        width: '180px',
        cell: (row) => <span>{row.transportFacility?row.transportFacility:'-'}</span>
    }
    // {
    //     name: "Men Only",
    //     selector: "men_only",
    //     sortable: true,
    //     center:true,
    //     width: '180px',
    //     cell: (row) => <span>{row.men_only?row.men_only:'-'}</span>
    // },
    // {
    //     name: "Women Only",
    //     selector: "women_only",
    //     sortable: true,
    //     center:true,
    //     width: '180px',
    //     cell: (row) => <span>{row.women_only?row.women_only:'-'}</span>
    // },
    // {
    //     name: "Distance Only",
    //     selector: "distance_only",
    //     sortable: true,
    //     center:true,
    //     width: '180px',
    //     cell: (row) => <span>{row.distance_only?row.distance_only:'-'}</span>
    // }
    ]

    const getInstitutionDetails = (id) => {
        axiosInstance
          .get("institutions/"+id+'/')
          .then((response) => {
            if (response.data.ok) {
                let details = JSON.parse(response.data.details);
                let degrees = response.data.degrees;
                let fees = JSON.parse(response.data.fees);
                let reviews = JSON.parse(response.data.reviews);
                let degree_program_dict = response.data.degree_program_dict;
                let accredAgency = JSON.parse(response.data.accred_agency)[0]['accredAgency'];
                let institute = JSON.parse(response.data.institute);
                setDegreeProgramDict(degree_program_dict);
                setFormData({...formData, ranking: reviews[0].ranking, rating: reviews[0].rating/2, reviews: reviews[0].numReviews, instituteName: institute[0].institutionName, accredBy: accredAgency})
                setDegreeOptions(degrees);
                setCurrInstitutionData({...currInstitutionData, details: details, fees: fees})
            //   setCurrInstitutionData(response.data.institution_data);
            } else {
              console.log("Error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
    };

    useEffect(() => {
        let id = window.location.href.split('/').at(-1);
        setCurrInstitutionId(id);
        getInstitutionDetails(id);
        if(session_is_auth){
            setIsAuth(true);
        }else{
            setIsAuth(false);
        }
        
        
    }, [session_is_auth])

    const changeRating = (newRating) => {
        setFormData({...formData, rating: newRating})
    }

    const redirectPage = (page) => {
        history.push(page);
    }

    const handleSelect = (eventKey) => {
        let tabDict = {
            'details': 1, 'reviews': 2, 'fees': 3, 'programmes': 4, 'admissions': 5, 'placements': 6
        }
        let tabList = Object.keys(tabDict);
        tabList.map(item => {
            if(item != eventKey){
                let ele = document.getElementById(eventKey);
                ele.style.setProperty('color', 'black', 'important');
            }
        })
        setTab(tabDict[eventKey]);
        let ele = document.getElementById(eventKey);
        let active_ele = document.getElementById('details');
        if(eventKey != 'details'){
            active_ele.style.setProperty('color', 'black', 'important');
        }
        if(ele){
            ele.style.setProperty('color', '#0C6EFD', 'important');
        }
    };


    const handleChange = (e, type, data) => {

        if(type === 'dropdown'){
          validate(data, e.value);
        }else{
          e.preventDefault();
          const { name, value } = e.target;
          validate(name, value);
        }
    
      };
    
      const validate = (name, value) => {
        switch (name) {
            case "degree":
                    setFormData({ ...formData, degree: value });
                    let programme_list = JSON.parse(degreeProgramDict[value]);
                    console.log(programme_list);
                    setProgramList(programme_list);
                if (value === 'Select Degree') {
                    setFormErrors({
                        ...formErrors,
                        degree: null,
                    });
                } else {
                    setFormErrors({ ...formErrors, degree: "" });
                }
                break;
            default:
                break;
        }
      };

      const formValid = (formErrors, formData) => {

        let valid = true;
        if(isAuth){
          // validate form errors being empty
          Object.keys(formErrors).forEach((key) => {
            formErrors[key].length > 0 && (valid = false);
          });
    
          // validate the form was filled out
          Object.keys(formData).forEach((key) => {
            formData[key] === null && (valid = false);
          });
    
        }else{
    
          // validate form errors being empty
          Object.values(formErrors).forEach((val) => {
            val.length > 0 && (valid = false);
          });
    
          // validate the form was filled out
          Object.values(formData).forEach((val) => {
            val === null && (valid = false);
          });
    
        }
    
        return valid;
      };
      
      const handleSubmit = (e) => {
    
        e.preventDefault();
    
        let post_data = {};
        let valid = false;
        if (formValid(formErrors, formData)) {
            valid = true;
            post_data = formData;
        } else {
            valid = false;
            console.error("FORM INVALID");
        }
        
        if (valid) {
            post_data["email"] = curr_user_data.email;
            post_data['id'] = curr_user_data.id;
            axiosInstance
                .put("edit_user/"+parseInt(curr_user_data.id)+"/", post_data)
                .then((response) => {
                if (response.data.ok) {
                    setIsFormSubmitError(false);
                    setFormData(response.data.user);
                    dispatch(user_data(response.data.session_user));
                } else {
                    setIsFormSubmitError(true);
                    console.log("Error");
                }
                })
                .catch((error) => {
                    setIsFormSubmitError(true);
                    console.log(error);
                });
        }
      };

    return (
            <Row style={{ padding: '0px', margin: '3% 0% 0% 0%' }}>
                <Col xs={12} sm={12} md={12} lg={12} xl={12} style={{ margin: '0px', padding: '0px'}}>
                    <Carousel>
                        <Carousel.Item className="carousel_container">
                            <img
                            className="d-block w-100 carousel_img"
                            src={carousel1}
                            alt="First slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item className="carousel_container">
                            <img
                            className="d-block w-100 carousel_img"
                            src={carousel2}
                            alt="Second slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item className="carousel_container">
                            <img
                            className="d-block w-100 carousel_img"
                            src={carousel3}
                            alt="Third slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item className="carousel_container">
                            <img
                            className="d-block w-100 carousel_img"
                            src={carousel4}
                            alt="Fourth slide"
                            />
                        </Carousel.Item>
                    </Carousel>
                </Col>
                <Col xs={12} sm={12} md={12} lg={12} xl={12} style={{ margin: '0px', padding: '0px'}}>
                <Card>
                    <Card.Body style={{ padding: '5px 0px', margin: '0px'}}>
                        <Card.Title style={{ margin: '10px 0px 20px 0px', padding: '0px'}}>
                            <Row style={{ padding: '0px', margin: '0px'}}>
                            <Col style={{ margin: '0px', padding: '0px'}} xs={12} sm={12} md={12} lg={12} xl={12}>
                                <div style={{ padding: '0px 0px 0px 0px', fontWeight: 'bold'}}>{formData.instituteName} (Rank - {formData.ranking})</div>
                            </Col>
                            </Row>
                            <Row style={{ padding: '0px', margin: '0px'}}>
                                <Col style={{ margin: '0px', padding: '0px'}} xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div style={{ padding: '0px 0px 0px 10px', fontSize: '12px',  float: 'left' }}><b>URL:</b> {currInstitutionData && currInstitutionData.hasOwnProperty('details') && currInstitutionData.details.length > 0 && currInstitutionData.details[0] && currInstitutionData.details[0].url?currInstitutionData.details[0].url:''}</div>
                                </Col>
                                <Col style={{ margin: '0px', padding: '10px 0px 0px 0px'}} xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div style={{ padding: '0px 10px 0px 0px', fontSize: '12px',  float: 'right' }}><b>Accredated By:</b> {formData.accredBy}</div>
                                </Col>
                            </Row>
                            <Row style={{ padding: '0px', margin: '0px'}}>
                                <Col style={{ margin: '0px', padding: '10px 0px 0px 0px'}} xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div style={{ padding: '0px 0px 0px 10px', fontSize: '12px',  float: 'left' }}><b>NPCURL: </b> {currInstitutionData && currInstitutionData.hasOwnProperty('details') && currInstitutionData.details.length > 0 && currInstitutionData.details[0] && currInstitutionData.details[0].url?currInstitutionData.details[0].npcUrl:''}</div>
                                </Col>
                            </Row>
                        </Card.Title>
                    </Card.Body>
                </Card>
                <Nav variant="tabs" defaultActiveKey="details" onSelect={handleSelect}>
                    <Nav.Item>
                        <Nav.Link style={{ color: '#0C6EFD' }} id="details" eventKey="details">Details</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link id="reviews" className="tabs" eventKey="reviews">Reviews</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link id="fees" className="tabs" eventKey="fees">Fees and Expenses</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link id="programmes" className="tabs" eventKey="programmes">Programmes and Courses</Nav.Link>
                    </Nav.Item>
                    {/* <Nav.Item>
                        <Nav.Link id="admissions" className="tabs" eventKey="admissions">Admissions</Nav.Link>
                    </Nav.Item> */}
                    {/* <Nav.Item>
                        <Nav.Link id="placements" className="tabs" eventKey="placements">Placements</Nav.Link>
                    </Nav.Item> */}
                </Nav>
                <Row style={{ padding: '0px', margin: '0px' }}>
                    {tab === 1?<Col style={{ padding: '0px', margin: '0px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                        <div className="row" style={{ padding: '0px', margin: '20px 0px' }}>
                            <div tyle={{ padding: '0px', margin: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <DataTable
                                    columns={details_columns}
                                    data={currInstitutionData.details}
                                    pagination
                                    paginationRowsPerPageOptions={[10, 15, 20, 25, 30]}
                                    paginationPerPage={10}
                                />
                            </div>
                        </div>
                    </Col>:""}
                    {tab === 2?<Col style={{ padding: '0px', margin: '0px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                    <div className="row" style={{ padding: '0px', margin: '50px 0px' }}>
                            <div tyle={{ padding: '0px', margin: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <p>Overall rating (Out of 5)</p>
                                <h1>{formData.rating/2}</h1>
                            </div>
                            <div tyle={{ padding: '0px', margin: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <Ratings
                                    rating={formData.rating/2}
                                    widgetRatedColors="green"
                                >
                                    <Ratings.Widget widgetHoverColor="green"  />
                                    <Ratings.Widget widgetHoverColor="green" />
                                    <Ratings.Widget widgetHoverColor="green" />
                                    <Ratings.Widget widgetHoverColor="green" />
                                    <Ratings.Widget widgetHoverColor="green" />
                                </Ratings>
                            </div>
                            <div tyle={{ padding: '0px', margin: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <p style={{ marginTop: '10px', fontSize: '14px' }}>Based on {formData.reviews} Verified Reviews</p>
                            </div>
                        </div>
                    </Col>:""}
                    {tab === 3?<Col style={{ padding: '0px', margin: '0px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                        <div className="row" style={{ padding: '0px', margin: '20px 0px' }}>
                            <div tyle={{ padding: '0px', margin: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <DataTable
                                    columns={fees_columns}
                                    data={currInstitutionData.fees}
                                    pagination
                                    paginationRowsPerPageOptions={[10, 15, 20, 25, 30]}
                                    paginationPerPage={10}
                                />
                            </div>
                        </div>
                    </Col>:""}
                    {tab === 4?<Col style={{ padding: '0px', margin: '0px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form className="row" style={{ padding: '20px 0px 0px 0px', margin: '0px' }} onSubmit={(e) => handleSubmit(e)}>
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <CSRFToken />
                                <Row style={{ margin: '0px', padding: '0px' }}>
                                    <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="degree">
                                        <Select
                                            value={degreeOptions.filter(({value}) => value === formData.degree)}
                                            onChange={value => handleChange(value, 'dropdown', 'degree')}
                                            placeholder={'Select Degree'}
                                            className="degree"
                                            classNamePrefix="select_degree"
                                            isSearchable={true}
                                            name="degree"
                                            options={degreeOptions}
                                        />
                                        <Row style={{ padding: "0px", margin: "0px" }}>
                                            <Col style={{ padding: "0px" }}>
                                                {formErrors.first_name && formErrors.first_name.length > 0 && (
                                                <span className="float-start error_message">
                                                    {formErrors.first_name}
                                                </span>
                                                )}
                                            </Col>
                                        </Row>
                                    </Form.Group>
                                </Row>
                            </div>
                        </Form>
                        <div style={{ padding: '0px', margin: '20px 0px 20px 0px' }} className="row">
                            <div style={{ padding: '0px', margin: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <h4 style={{ marginBottom: '10px', color: '#0C6EFD' }}>Programs Offered</h4>
                                <ul className="list-group">
                                    {programList.map((item, idx) => {
                                        return (
                                            <li style={{ marginLeft: '33.33%', width: '33.33%' }} key={idx} className="list-group-item">{item.programmeDesc}</li>
                                        )
                                        })
                                    }
                                </ul>           
                            </div>
                        </div>
                    </Col>:""}
                </Row>
                </Col>
            </Row>
    );
    }

export default withRouter(Home);
