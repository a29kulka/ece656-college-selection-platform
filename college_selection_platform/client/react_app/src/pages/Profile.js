import React, { useState, useEffect, useRef } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Badge from 'react-bootstrap/Badge'
import Select from 'react-select'
import Datetime from 'react-datetime'
import "react-datetime/css/react-datetime.css";
import axios from "axios";
import { useHistory, withRouter } from "react-router-dom";
import { user_data } from "../redux";
import { useDispatch, useSelector } from "react-redux";
import CSRFToken from "../components/Csrf";
import "../index.css";

import axiosInstance from "../components/AxiosInstance";
import { API_URL } from "../constants"

const Profile = (props) => {

  axios.defaults.xsrfCookieName = "csrftoken";
  axios.defaults.xsrfHeaderName = "X-CSRFToken";

  const [windowDimensions, setWindowDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });
  const history = useHistory();
  const [signUpSuccessMsg, setSignUpSuccessMsg] = useState("");
  const [genderOptions, setGenderOptions] = useState([{'label': 'Male', 'value': 'Male'},{'label': 'Female', 'value': 'Female'}, {'label': 'Other', 'value': 'Other'}]);
  const [countryOptions, setCountryOptions] = useState([{'label': 'India', 'value': 'India'}]);
  const [roleOptions, setRoleOptions] = useState([{'label': 'Student', 'value': 'Student'},{'label': 'Teacher', 'value': 'Teacher'}, {'label': 'InstituteRepresentative', 'value': 'InstituteRepresentative'}])
  
  const [preSelectedCountry, setPreSelectedCountry] = useState([])
  const [preSelectedGender, setPreSelectedGender] = useState([])
  const genderDropDownRef = useRef(null);
  const countryDropDownRef = useRef(null);
  const institutionDropdownRef = useRef(null);
  const roleDropdownRef = useRef(null);
  const [isFormSubmitError, setIsFormSubmitError] = useState(false);
  const [errorList, setErrorList] = useState([]);
  const [adminEditMode, setAdminEditMode] = useState(false);
  const dispatch = useDispatch();
  const [isDisabled, setIsDisabled] = useState(false);
  const [editMode, setEditMode] = useState(true);
  const [formData, setFormData] = useState({
    'first_name':'',
    'last_name':'',
    'email':'',
    'username':'',
    'password':'',
    'role':'',
    'country':'',
    'state':'',
    'city':'',
    'zip_code':''
  });
  const [formErrors, setFormErrors] = useState({
    'first_name':'',
    'last_name':'',
    'email':'',
    'username':'',
    'password':'',
    'role':'',
    'country':'',
    'state':'',
    'city':'',
    'zip_code':''
  });

  const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );

  
  const curr_user_data = useSelector((state) => state.session.userData);
  const session_is_auth = useSelector((state) => state.session.isLoggedIn);
  const [isAuth, setIsAuth] = useState(false);

  useEffect(() => {
    getAllCountries();
    if(history.location.pathname === '/profile'){
        setEditMode(true);
        let userData = {
          'id': curr_user_data['id'],
          'first_name':curr_user_data['firstName'],
          'last_name':curr_user_data['lastName'],
          'email':curr_user_data['email'],
          'username':curr_user_data['username'],
          'role':curr_user_data['role'],
          'country':curr_user_data['country'],
          'state':curr_user_data['state'],
          'city':curr_user_data['city'],
          'zip_code':curr_user_data['zipCode'],
        }
        setFormData(userData);
    }else if(history.location.pathname.includes('/admin_dashboard/users/')){
        let id = history.location.pathname.split("/").at(-1);
        getUserDetails(id);
        setAdminEditMode(true);
    }else{
      setAdminEditMode(false);
        setEditMode(false);
    }
  }, [history])

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside, false);
    if(session_is_auth){
        setIsAuth(true);
    }else{
        setIsAuth(false);
    }
  }, [session_is_auth])


  const getUserDetails = (id) => {
    axiosInstance
    .get("get_user_details/"+id+"/")
    .then((response) => {
      let curr_user_data = response.data.user;
      let userData = {
        'id': curr_user_data['id'],
        'first_name':curr_user_data['firstName'],
        'last_name':curr_user_data['lastName'],
        'email':curr_user_data['email'],
        'username':curr_user_data['username'],
        'role':curr_user_data['role'],
        'country':curr_user_data['country'],
        'state':curr_user_data['state'],
        'city':curr_user_data['city'],
        'zip_code':curr_user_data['zipCode'],
      }
      setFormData(userData);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  const getAllCountries = () => {
    axiosInstance
    .get("all_countries/")
    .then((response) => {
        let country_arr = response.data.countries;
        let countries = country_arr.map(item => {
          return {'value': item.name, 'label': item.name}
        })
        setCountryOptions(countries);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  const handleClickOutside = (e) => {
      let elements = [...document.getElementsByClassName("optionListContainer")];
      let curr_gender_ele = null;
      let curr_country_ele = null;
      let curr_institution_ele = null;
      elements.map(item => {
        if(item.parentElement.id === "gender"){
          curr_gender_ele = item;
        }
        if(item.parentElement.id === "country"){
          curr_country_ele = item;
        }
        if(item.parentElement.id === "institution"){
            curr_institution_ele = item;
        }
        
      });

      if (
        genderDropDownRef &&
        e.target &&
        genderDropDownRef.current && !genderDropDownRef.current.contains(e.target)
      ) {
        if(curr_gender_ele){
          curr_gender_ele.style.display = 'none';
        }
      }else{
        if(curr_gender_ele){
          curr_gender_ele.style.display = 'block';
        }
      }

      
      if (
        countryDropDownRef &&
        e.target &&
        countryDropDownRef.current && !countryDropDownRef.current.contains(e.target)
      ) {
        if(curr_country_ele){
          curr_country_ele.style.display = 'none';
        }
      }else{
        if(curr_country_ele){
          curr_country_ele.style.display = 'block';
        }
      }

      if (
        institutionDropdownRef &&
        e.target &&
        institutionDropdownRef.current && !institutionDropdownRef.current.contains(e.target)
      ) {
        if(curr_institution_ele){
            curr_institution_ele.style.display = 'none';
        }
      }else{
        if(curr_institution_ele){
            curr_institution_ele.style.display = 'block';
        }
      }

  };

  const redirectPage = (page) => {
      history.push(page);
  }

  const formValid = (formErrors, formData) => {

    let valid = true;
    if(isAuth){
      // validate form errors being empty
      Object.keys(formErrors).forEach((key) => {
        if(key !== 'email' || key !== 'password' || key !== 'confirm_password'){
          formErrors[key].length > 0 && (valid = false);
        }
      });
    }else{
      // validate form errors being empty
      Object.values(formErrors).forEach((val) => {
        val.length > 0 && (valid = false);
      });
    }

    return valid;
  };


  const handleSubmit = (e, type, data, is_captcha_verified) => {
    
    e.preventDefault();

    
    let post_data = {};
    let valid = false;
    if (formValid(formErrors, formData)) {
        valid = true;
        post_data = formData;
    } else {
        valid = false;
    setSignUpSuccessMsg("Some of your fields are empty or incorrect");
    setTimeout(function () {
        setSignUpSuccessMsg("");
    }, 10000);
    console.error("FORM INVALID");
    }
    
    if (valid) {
        setIsDisabled(true);
        post_data['id'] = formData.id;    
        axiosInstance
            .post("edit_user/", post_data)
            .then((response) => {
            setIsDisabled(false);
            if (response.data.ok) {
                setIsFormSubmitError(false);
                setFormData(response.data.user);
                dispatch(user_data(response.data.session_user));
                setSignUpSuccessMsg("User modified successfully.");
                setTimeout(function () {
                    setSignUpSuccessMsg("");
                }, 10000);
                if(adminEditMode){
                  history.push('/admin_dashboard');
                }
            } else {
                setIsFormSubmitError(true);
                setSignUpSuccessMsg(response.data.error);
                setTimeout(function () {
                setSignUpSuccessMsg("");
                }, 10000);
                console.log("Error");
            }
            })
            .catch((error) => {
            setIsDisabled(false);
            setIsFormSubmitError(true);
            setSignUpSuccessMsg("Some error Occurred");
            setTimeout(function () {
                setSignUpSuccessMsg("");
            }, 10000);
            console.log(error);
        });
    }
  };

  const handleChange = (e, type, data) => {

    let password = formData.password;
    let confirm_password = formData.confirm_password;
    let email = formData.email;
    let confirm_email = formData.confirm_email;

    if(type === 'dropdown'){
      validate(data, e.value, email, password, confirm_password, confirm_email);
    }else{
      e.preventDefault();
      const { name, value } = e.target;
      validate(name, value, email, password, confirm_password, confirm_email);
    }

  };

  const validate = (name, value, email, password, confirm_password, confirm_email) => {
    switch (name) {
      case "email":
          setFormData({ ...formData, email: value });
          if (emailRegex.test(value)) {
              setFormErrors({ ...formErrors, email: "" });
          } else {
            if (value.length > 0) {
              setFormErrors({ ...formErrors, email: "Invalid email address" });
            } else {
                setTimeout(function () {
                    setFormErrors({ ...formErrors, email: "" });
                }, 2000);
            }
          }
          break;
          case "first_name":
            setFormData({ ...formData, first_name: value });
          break;
          case "last_name":
            setFormData({ ...formData, last_name: value });
          break;
      case "username":
            setFormData({ ...formData, username: value });
            if (value.length <= 0) {
              setFormErrors({ ...formErrors, username: "Invalid username" });
            } else if(value.length > 10){
              setFormErrors({ ...formErrors, username: "username must have less than 10 characters." });
            }else {
              setFormErrors({ ...formErrors, username: "" });
            }
          break;
      case "city":
            setFormData({ ...formData, city: value });
          break;
      case "state":
            setFormData({ ...formData, state: value });
          break;
      case "zip_code":
            setFormData({ ...formData, zip_code: value });
          break;
      case "graduate":
        setFormData({ ...formData, graduate: value });
        break;
      case "under_graduate":
        setFormData({ ...formData, under_graduate: value });
        break;
      case "highschool":
        setFormData({ ...formData, highschool: value });
          break;
      case "country":
            setFormData({ ...formData, country: value });
            if (value === 'Select Country') {
              setFormErrors({
                ...formErrors,
                country: null,
              });
            } else {
              setFormErrors({ ...formErrors, country: "" });
            }
        break;
      default:
        break;
    }
  };

  
  const spinner = (display) => {
    display
      ? (document.getElementById("overlay").style.display = "block")
      : (document.getElementById("overlay").style.display = "none");
  };

  const spinnerStop = () => {
    spinner(false);
  };


  return (
    <Row style={{ margin: '8% 0% 0% 0%', padding: '0px', border: '1px solid lightgray' }}>
        <Col style={{ margin: '15px 0px 0px 0px', padding: '0px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
            <div style={{ fontSize:'1.5rem'}}>Profile</div>
        </Col>
        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
          <Form style={{ padding: '0px 100px 20px 100px' }} onSubmit={(e) => handleSubmit(e, "login", "")}>
            <CSRFToken />
            <Row style={{ margin: '0px 0px 0px -20px', padding: '0px 0px 10px 0px' }}>
                <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <div style={{ fontSize:'1.2rem', float: 'left', fontWeight: 'bold' }}>Personal Information</div>    
                </Col>    
            </Row>
            <Row style={{ margin: '0px', padding: '0px' }}>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id="first_name">
                    <Form.Label className="float-start">First Name</Form.Label>
                    <Form.Control
                        name="first_name"
                        value={formData.first_name}
                        onChange={(e) => handleChange(e)}
                        type="text"
                        placeholder="Enter First Name"
                    />
                    <Row style={{ padding: "0px", margin: "0px" }}>
                    <Col style={{ padding: "0px" }}>
                        {formErrors.first_name && formErrors.first_name.length > 0 && (
                        <span className="float-start error_message">
                            {formErrors.first_name}
                        </span>
                        )}
                    </Col>
                    </Row>
                </Form.Group>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id="last_name">
                    <Form.Label className="float-start">Last Name</Form.Label>
                    <Form.Control
                    name="last_name"
                    value={formData.last_name}
                    onChange={(e) => handleChange(e)}
                    type="text"
                    placeholder="Enter Last Name"
                    />
                    <Row style={{ padding: "0px", margin: "0px" }}>
                    <Col style={{ padding: "0px" }}>
                        {formErrors.last_name && formErrors.last_name.length > 0 && (
                        <span className="float-start error_message">
                            {formErrors.last_name}
                        </span>
                        )}
                    </Col>
                    </Row>
                </Form.Group>
            </Row>
            <Row style={{ margin: '0px', padding: '0px' }}>
              <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id="email">
                <Form.Label className="float-start">Email Address </Form.Label>
                <Form.Control
                    name="email"
                    type="email"
                    placeholder="Enter Email Address"
                    value={formData.email}
                    onChange={(e) => handleChange(e)}
                    >
                </Form.Control>
                <Row style={{ padding: "0px", margin: "0px" }}>
                    <Col style={{ padding: "0px" }}>
                    {formErrors.email && formErrors.email.length > 0 && (
                    <span className="float-start error_message">
                        {formErrors.email}
                    </span>
                    )}
                    </Col>
                </Row>
                </Form.Group>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id="username">
                  <Form.Label className="float-start">Username <span style={{ color: 'red'}}>*</span></Form.Label>
                  <Form.Control
                      name="username"
                      type="username"
                      placeholder="Enter Username"
                      value={formData.username}
                      onChange={(e) => handleChange(e)}
                      required
                      >
                  </Form.Control>
                  <Row style={{ padding: "0px", margin: "0px" }}>
                      <Col style={{ padding: "0px" }}>
                      {formErrors.username && formErrors.username.length > 0 && (
                      <span className="float-start error_message">
                          {formErrors.username}
                      </span>
                      )}
                      </Col>
                  </Row>
                </Form.Group>
            </Row>
            <Row style={{ margin: '0px', padding: '0px' }}>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="country" ref={countryDropDownRef}>
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form.Label className="float-start">Country</Form.Label>
                        </Col>
                    </Row>
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Select
                            value={countryOptions.filter(({value}) => value === formData.country)}
                            onChange={value => handleChange(value, 'dropdown', 'country')}
                            placeholder={'Select Country'}
                            className="country"
                            classNamePrefix="select_country"
                            // defaultValue={preSelectedCountry}
                            isSearchable={true}
                            name="country"
                            options={countryOptions}
                        />
                        </Col>
                    </Row>
                    </Form.Group>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="state">
                        <Form.Label className="float-start">State</Form.Label>
                        <Form.Control
                        name="state"
                        value={formData.state}
                        onChange={(e) => handleChange(e)}
                        type="text"
                        placeholder="Enter State"
                        />
                        <Row style={{ padding: "0px", margin: "0px" }}>
                        <Col style={{ padding: "0px" }}>
                            {formErrors.state && formErrors.state.length > 0 && (
                            <span className="float-start error_message">
                                {formErrors.state}
                            </span>
                            )}
                        </Col>
                        </Row>
                    </Form.Group>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="city">
                    <Form.Label className="float-start">City</Form.Label>
                    <Form.Control
                    name="city"
                    value={formData.city}
                    onChange={(e) => handleChange(e)}
                    type="text"
                    placeholder="Enter City"
                    />
                    <Row style={{ padding: "0px", margin: "0px" }}>
                    <Col style={{ padding: "0px" }}>
                        {formErrors.city && formErrors.city.length > 0 && (
                        <span className="float-start error_message">
                            {formErrors.city}
                        </span>
                        )}
                    </Col>
                    </Row>
                </Form.Group>
            </Row>
        
            <Row style={{ margin: '0px', padding: '0px' }}>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id="zip_code">
                    <Form.Label className="float-start">Zip Code</Form.Label>
                    <Form.Control
                    name="zip_code"
                    value={formData.zip_code}
                    onChange={(e) => handleChange(e)}
                    type="text"
                    placeholder="Enter Zip Code"
                    />
                    <Row style={{ padding: "0px", margin: "0px" }}>
                    <Col style={{ padding: "0px" }}>
                        {formErrors.zip_code && formErrors.zip_code.length > 0 && (
                        <span className="float-start error_message">
                            {formErrors.zip_code}
                        </span>
                        )}
                    </Col>
                    </Row>
                </Form.Group>

                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id="role" ref={roleDropdownRef}>
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Label className="float-start">Role</Form.Label>
                        </Col>
                        </Row>
                        <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Control
                                name="role"
                                type="text"
                                value={formData.role}
                                disabled
                            >
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>
                
            </Row>   
            {/* <Row style={{ margin: '0px', padding: '0px' }}>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id="last_login">
                  <Row style={{ margin: '0px', padding: '0px' }}>
                    <Col style={{ margin: '0px', padding: '0px 10px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Form.Label className="float-start">
                        <div style={{ paddingTop: '25px' }}>Last Login
                        <Badge style={{ margin: '0px 0px 0px 10px'}} bg="primary">{formData.last_login}</Badge>
                        </div>
                      </Form.Label>
                    </Col>
                  </Row>
                </Form.Group>
            </Row>      */}
            {/* <Row style={{ margin: '20px 0px 0px -20px', padding: '0px 0px 10px 0px' }}>
                <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <div style={{ fontSize:'1.2rem', float: 'left', fontWeight: 'bold' }}>Educational Information</div>    
                </Col>   
            </Row>
            <Row style={{ margin: '10px 0px 0px 0px', padding: '0px' }}>
                <Form.Group className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="education_level">
                    <div className="float-start">
                        <Form.Check 
                            inline
                            type='checkbox'
                            id='graduate_checkbox'
                            label='Graduate'
                            name="graduate"
                            checked={formData.graduate}
                            onChange={() => setFormData({...formData, graduate: !formData.graduate})}
                        />
                        <Form.Check 
                            inline
                            type='checkbox'
                            id='under_graduate_checkbox'
                            label='UnderGraduate'
                            name="under_graduate"
                            checked={formData.under_graduate}
                            onChange={() => setFormData({...formData, under_graduate: !formData.under_graduate})}
                        />
                        <Form.Check 
                            inline
                            type='checkbox'
                            id='highschool'
                            label='HighSchool'
                            name="highschool"
                            checked={formData.highschool}
                            onChange={() => setFormData({...formData, highschool: !formData.highschool})}
                        />
                    </div>
                </Form.Group>
            </Row> 
            {formData.graduate || formData.under_graduate || formData.highschool?
            <Row style={{ margin: '20px 0px 0px 0px', padding: '0px' }}>
                {formData.graduate?<Form.Group style={{ margin: '0px', padding: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="graduate_school_name">
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 0px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Label className="float-start">Graduate School Name</Form.Label>
                        </Col>
                        </Row>
                        <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Control
                                name="graduate_school_name"
                                type="text"
                                value={formData.graduate_school_name}
                            >
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>:""}
                {formData.under_graduate?<Form.Group style={{ margin: '0px', padding: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="under_graduate_school_name">
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 0px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Label className="float-start">Under Graduate School Name</Form.Label>
                        </Col>
                        </Row>
                        <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Control
                                name="under_graduate_school_name"
                                type="text"
                                value={formData.under_graduate_school_name}
                            >
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>:""}
                {formData.highschool?<Form.Group style={{ margin: '0px', padding: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="highschool_name">
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 0px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Label className="float-start">High School Name</Form.Label>
                        </Col>
                        </Row>
                        <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Control
                                name="highschool_name"
                                type="text"
                                value={formData.highschool_name}
                                
                            >
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>:""}
            </Row>
            :""}
            {formData.graduate || formData.under_graduate || formData.highschool?
            <Row style={{ margin: '20px 0px 20px 0px', padding: '0px' }}> */}
            {/* {formData.graduate?<Form.Group style={{ margin: '0px', padding: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="graduate_school_marks_obtained">
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 0px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Label className="float-start">Graduate Marks Obtained</Form.Label>
                        </Col>
                        </Row>
                        <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Control
                                name="graduate_school_marks_obtained"
                                type="text"
                                value={formData.graduate_school_marks_obtained}
                                
                            >
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>:""}
                {formData.under_graduate?<Form.Group style={{ margin: '0px', padding: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="under_graduate_marks_obtained">
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 0px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Label className="float-start">Under Graduate  Marks Obtained</Form.Label>
                        </Col>
                        </Row>
                        <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Control
                                name="under_graduate_marks_obtained"
                                type="text"
                                value={formData.under_graduate_marks_obtained}
                                
                            >
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>:""}
                {formData.highschool?<Form.Group style={{ margin: '0px', padding: '0px' }} className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" id="highschool_marks_obtained">
                    <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 0px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Label className="float-start">High School Marks Obtained</Form.Label>
                        </Col>
                        </Row>
                        <Row style={{ margin: '0px', padding: '0px' }}>
                        <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                            <Form.Control
                                name="highschool_marks_obtained"
                                type="text"
                                value={formData.highschool_marks_obtained}
                                
                            >
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>:""}
            </Row>
            :""} */}
            <Row style={{ margin: '0px 0px 0px 0px',  padding: "0px 10px"  }}>
                <Col style={{ margin: '0px', padding: '0px' }}>
                    <Button
                        size="md"
                        className="btn-block btn-primary"
                        type="submit"
                    >
                    <span>Update</span>
                    </Button>
                </Col>
            </Row>
            {!signUpSuccessMsg ? (
                ""
            ) : (
                <Row style={{ margin: "10px 0px 10px 0px", padding: "0px" }}>
                <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <div
                    style={{ textAlign: 'left' }}
                    className={isFormSubmitError?"form_error_message":"form_success_message"}
                    >
                    {signUpSuccessMsg}
                    </div>
                </Col>
                </Row>
            )}
          </Form>
        </Col>
      </Row>
  );
}

export default withRouter(Profile);

