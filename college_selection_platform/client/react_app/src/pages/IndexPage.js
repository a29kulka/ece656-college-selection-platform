import React, { useState, useEffect } from "react";
import { withRouter, useHistory } from "react-router-dom";
import '../static/css/custom.css'
import SignIn from "./SignIn";
import SignUp from "./Registration";
import $, { data } from 'jquery'
import logo from '../static/images/logo.svg';
import landing_img from '../static/images/landing_img.png';
import { Row, Col, Form, FormInput, Button } from "react-bootstrap";
import ForgotPassword from "./ForgotPassword";
import AdminDashboard from "./AdminDashboard";

const IndexPage = (props) => {
  
  const history = useHistory();

  const [pageStatus, setPageStatus] = useState({});

  const [switchClick, setSwitchClick] = useState("login");

  const [emailData, setEmailData] = useState("");

  const [dataContent, setDataContent] = useState("Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?");

  const [socialSignup, setSocialSignup] = useState("");

  useEffect(() => {
    //
  }, [socialSignup])

  useEffect(() => { 
    let path = history.location.pathname;
    if(path === '/' || path === '/signin' || path === '/signin/'){
      setPageStatus({'signin': true, 'signup': false, 'access_code': false, 'forgot_password': false, 'admin': false});
    }else if(path === '/signup' || path === '/signup/'){
      setPageStatus({'signin': false, 'signup': true, 'access_code': false, 'forgot_password': false, 'admin': false});
    }else if(path === '/access_code' || path === '/access_code/'){
      setPageStatus({'signin': false, 'signup': false, 'access_code': true, 'forgot_password': false, 'admin': false});
    }else if(path === '/forgot_password' || path === '/forgot_password/'){
      setPageStatus({'signin': false, 'signup': false, 'access_code': false, 'forgot_password': true, 'admin': false});
    }else if(path === '/admin' || path === '/admin/'){
      setPageStatus({'signin': false, 'signup': false, 'access_code': false, 'forgot_password': false, 'admin': true});
    }
  }, [history.pathname])


  const redirectPage = (page) => {
    history.push('/signup');
  }

  const emailDataMethod = (data) => {
    setEmailData(data);
  }


  return (
    <Row style={{ margin: '0px 0px 0px 0px', padding: '0px', backgroundColor: 'white' }}>
      <Col lg="6" md="12" sm="12" xs="12">
        <img src={landing_img} style={{ margin:'15% 0% 0% 6%', height: '400px', width: '600px' }}></img>
      </Col>
      <Col lg="6" md="12" sm="12" xs="12">
        <Row style={{ padding: '0px', margin: '40px 0px 0px 0px'}}>
          <Col lg="12" md="12" sm="12" xs="12">
            <img src={logo} style={{ height: '80px', width: '80px' }}></img>
          </Col>
        </Row>
        {pageStatus.signin?<SignIn/>:''}
        {pageStatus.signup?<SignUp/>:''}
        {pageStatus.admin?<SignIn/>:''}
      </Col>
    </Row>

  );
}

export default withRouter(IndexPage);
