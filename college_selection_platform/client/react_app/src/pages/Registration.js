import React, { useState, useEffect, useRef } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import Alert from "react-bootstrap/Alert";
import Container from "react-bootstrap/Container";
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import Image from 'react-bootstrap/Image'
import Modal from 'react-bootstrap/Modal'
import { Orientation } from "../components/Orientation";
import { Multiselect } from 'multiselect-react-dropdown';
import Select from 'react-select'
import $ from 'jquery'
import {
  FaUser,
  FaWallet,
  FaAddressCard,
  FaFileAlt,
  FaLaptop,
  FaEye,
  FaEyeSlash,
  FaArrowLeft,
  FaExclamationTriangle,
} from "react-icons/fa";
import axios from "axios";
import { useHistory, withRouter, useParams } from "react-router-dom";
import { user_created_success, user_data } from "../redux";
import { useDispatch, useSelector } from "react-redux";
import CSRFToken from "../components/Csrf";
import "../index.css";
import {isMobile, isMobileOnly} from 'react-device-detect';
import {
    clear_session,
  } from "../redux";
import axiosInstance from "../components/AxiosInstance";
import { API_URL } from "../constants"

const Registration = (props) => {

  axios.defaults.xsrfCookieName = "csrftoken";
  axios.defaults.xsrfHeaderName = "X-CSRFToken";

  const history = useHistory();
  const [orientation, setOrientation] = useState("default");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [signUpSuccessMsg, setSignUpSuccessMsg] = useState("");
  const [roleOptions, setRoleOptions] = useState([{'label': 'Admin', 'value': 'Admin'}, {'label': 'Student', 'value': 'Student'}])
  const institutionDropdownRef = useRef(null);
  const roleDropdownRef = useRef(null);
  const [isFormSubmitError, setIsFormSubmitError] = useState(false);
  const [errorList, setErrorList] = useState([]);
  const dispatch = useDispatch();
  const [isDisabled, setIsDisabled] = useState(false);
  const [formData, setFormData] = useState({
    'username':'',
    'email':'',
    'password':'',
    'confirm_password':'',
    'role':''
  });
  const [formErrors, setFormErrors] = useState({
    'username':'',
    'email':'',
    'password':'',
    'confirm_password':'',
    'role':''
  });
  const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );
  const passwordRegex = RegExp(
    /^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*[^\w\s])\S{6,}$/
  );

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside, false);
  }, [])

  const handleClickOutside = (e) => {
      let elements = [...document.getElementsByClassName("optionListContainer")];
      let curr_gender_ele = null;
      let curr_country_ele = null;
      let curr_company_ele = null;
      let curr_university_ele = null;
      let curr_billing_country_ele = null;
      elements.map(item => {
        
        if(item.parentElement.id === "institution"){
          curr_company_ele = item;
        }
        
      });

      if (
        institutionDropdownRef &&
        e.target &&
        institutionDropdownRef.current && !institutionDropdownRef.current.contains(e.target)
      ) {
        if(curr_country_ele){
          curr_country_ele.style.display = 'none';
        }
      }else{
        if(curr_country_ele){
          curr_country_ele.style.display = 'block';
        }
      }

  };


  const redirectPage = (page) => {
      history.push(page);
  }

  const formValid = (formErrors, formData) => {

      let valid = true;

      // validate form errors being empty
      Object.values(formErrors).forEach((val) => {
        val.length > 0 && (valid = false);
      });

      // validate the form was filled out
      Object.values(formData).forEach((val) => {
        val === null && (valid = false);
      });

    return valid;
  };

  

  const handleSubmit = (e, type, data, is_captcha_verified) => {


    e.preventDefault();

    let post_data = {};
    let valid = false;
    if (type === "sso") {
      post_data = data;
      valid = true;
    } else {
      if (formValid(formErrors, formData)) {
        valid = true;
        post_data = formData;
      } else {
        valid = false;
        setSignUpSuccessMsg("Some of your fields are empty or incorrect");
        setTimeout(function () {
          setSignUpSuccessMsg("");
        }, 10000);
        console.error("FORM INVALID");
      }
    }
    if (valid) {
      if (post_data.hasOwnProperty("confirm_password")) {
        delete post_data["confirm_password"];
      }
      axios
      .post(API_URL+"register/", post_data)
      .then((response) => {
        if (response.data.ok) {
          setIsFormSubmitError(false);
          history.push("/signin");
          if(post_data['sso']){
            dispatch(user_created_success('sso_login'));
          }else{
            dispatch(user_created_success('login'));
          }
        } else {
          setIsFormSubmitError(true);
          let msg = '';
          Object.values(response.data.error).map(item => {
              msg = item.map(innerItem => {
                return innerItem + '\n';
              })
          })
          setSignUpSuccessMsg(msg);
          setTimeout(function () {
            setSignUpSuccessMsg("");
          }, 10000);
          console.log("Error");
        }
      })
      .catch((error) => {
        setIsFormSubmitError(true);
        setSignUpSuccessMsg("Some error Occurred");
        setTimeout(function () {
          setSignUpSuccessMsg("");
        }, 10000);
        console.log(error);
    });
    }
  
  };


  const handleChange = (e, type, data) => {

    let password = formData.password;
    let confirm_password = formData.confirm_password;
    let email = formData.email;
    let confirm_email = formData.confirm_email;

    if(type === 'dropdown'){
      validate(data, e.value, email, password, confirm_password, confirm_email);
    }else{
      e.preventDefault();
      const { name, value } = e.target;
      validate(name, value, email, password, confirm_password, confirm_email);
    }

  };

  const validate = (name, value, email, password, confirm_password, confirm_email) => {
    switch (name) {
      case "email":
          setFormData({ ...formData, email: value });
          if (emailRegex.test(value)) {
              setFormErrors({ ...formErrors, email: "" });
          } else {
            if (value.length > 0) {
              setFormErrors({ ...formErrors, email: "Invalid email address" });
            } else {
              setFormErrors({ ...formErrors, email: "" });
            }
          }
          break;
        case "username":
          setFormData({ ...formData, username: value });
          if (value.length <= 0) {
            setFormErrors({ ...formErrors, username: "Invalid username" });
          }else if(value.length > 10){
            setFormErrors({ ...formErrors, username: "username must have less than 10 characters." });
          }else {
            setFormErrors({ ...formErrors, username: "" });
          }
          break;
      case "password":
        setFormData({ ...formData, password: value });
        if (passwordRegex.test(value)) {
          if(confirm_password.length > 0){
            if(value === confirm_password){
              setFormErrors({ ...formErrors,  password: "" , confirm_password: ""});
            }else{
              setFormErrors({ ...formErrors, password: "Password and Confirm password should be the same" });
            }
          }else{
            setFormErrors({ ...formErrors, password: "" });
          }
        } else {
          if (value.length > 0) {
            setFormErrors({
              ...formErrors,
              password: "password must contain minimum 6, at least one uppercase letter, one lowercase letter, one number and one special character",
            });
          } else {
            setFormErrors({ ...formErrors, password: "" });
          }
        }
        break;
      case "confirm_password":
        setFormData({ ...formData, confirm_password: value });
        if (value.length > 0 && value !== password) {
          setFormErrors({
            ...formErrors,
            confirm_password: "Password and Confirm password should be the same",
          });
        } else {
          setFormErrors({ ...formErrors, confirm_password: "" });
        }
        break;
      case "role":
        setFormData({ ...formData, role: value });
        if (value === 'Select Role') {
          setFormErrors({
            ...formErrors,
            gender: "Select a role",
          });
        } else {
          setFormErrors({ ...formErrors, role: "" });
        }
    break;
      default:
        break;
    }
  };

  const spinner = (display) => {
    display
      ? (document.getElementById("overlay").style.display = "block")
      : (document.getElementById("overlay").style.display = "none");
  };

  const spinnerStop = () => {
    spinner(false);
  };

  return (
    <Row>
        <Col style={{ margin: '10px 0px 10px 0px' }} lg="12" md="12" sm="12" xs="12">
          <span style={{ fontWeight: 'bold', fontStyle: 'normal', fontFamily: 'Roboto', fontSize: '25px', lineHeight: '40px' }} >SIGN UP</span>
        </Col>
        <Col g="12" md="12" sm="12" xs="12">
          <Form style={{ padding: '20px 100px' }} onSubmit={(e) => handleSubmit(e, "login", "")}>
              <CSRFToken />
              <Form.Group id="username">
                  <Form.Label className="float-start">Username <span style={{ color: 'red'}}>*</span></Form.Label>
                  <Form.Control
                      name="username"
                      type="username"
                      placeholder="Enter Username"
                      value={formData.username}
                      onChange={(e) => handleChange(e)}
                      required
                      >
                  </Form.Control>
                  <Row style={{ padding: "0px", margin: "0px" }}>
                      <Col style={{ padding: "0px" }}>
                      {formErrors.username && formErrors.username.length > 0 && (
                      <span className="float-start error_message">
                          {formErrors.username}
                      </span>
                      )}
                      </Col>
                  </Row>
                </Form.Group>

              <Form.Group id="register_password">
                <Form.Label className="float-start">Password <span style={{ color: 'red'}}>*</span></Form.Label>
                <Form.Control
                    autoComplete="off"
                    name="password"
                    type="password"
                    value={formData.password}
                    onChange={(e) => handleChange(e)}
                    required
                  >
                </Form.Control>
                <Row
                  style={{ margin: "8px 0px 0px 0px", padding: "0px" }}
                  >
                  <Col sm={12} xs={12} md={12} style={{ padding: "0px" }}>
                      {formErrors.password.length > 0 && (
                      <span className="float-start error_message">
                          {formErrors.password}
                      </span>
                      )}
                  </Col>
                  </Row>
            </Form.Group>
            <Form.Group id="confirm_password">
               <Form.Label className="float-start">Confirm Password <span style={{ color: 'red'}}>*</span></Form.Label>
                  <Form.Control
                    autoComplete="off"
                    name="confirm_password"
                    type= "password"
                    value={formData.confirm_password}
                    onChange={(e) => handleChange(e)}
                    required
                  ></Form.Control>
                <Row style={{ padding: "0px", margin: "0px" }}>
                  <Col style={{ padding: "0px" }}>
                    {formErrors.confirm_password && formErrors.confirm_password.length > 0 &&
                      formErrors.password.length <= 0 && (
                        <span className="float-start error_message">
                          {formErrors.confirm_password}
                        </span>
                      )}
                  </Col>
                </Row>
              </Form.Group>
              <Form.Group id="role" ref={roleDropdownRef}>
                  <Row style={{ margin: '0px', padding: '0px' }}>
                    <Col style={{ margin: '0px', padding: '10px 10px 0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Form.Label className="custom_label_country float-start">Role <span style={{ color: 'red'}}>*</span></Form.Label>
                    </Col>
                  </Row>
                  <Row style={{ margin: '0px', padding: '0px' }}>
                    <Col style={{ margin: '0px', padding: '0px 10px' }} xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Select
                        value={roleOptions.filter(({value}) => value === formData.role)}
                        onChange={value => handleChange(value, 'dropdown', 'role')}
                        className="role"
                        classNamePrefix="select_role"
                        placeholder={false}
                        isSearchable={true}
                        name="role"
                        options={roleOptions}
                      />
                    </Col>
                  </Row>
                </Form.Group>
              <Row style={{ margin: '25px 0px 0px 0px',  padding: "0px 10px"  }}>
                  <Col style={{ margin: '0px', padding: '0px' }}>
                      <Button
                          size="lg"
                          className="btn-block btn-primary"
                          type="submit"
                      >
                      <span style={{ fontSize: '16px', fontWeight: 'bolder' }}>SIGN UP</span>
                      </Button>
                  </Col>
              </Row>
              {!signUpSuccessMsg ? (
                  ""
                ) : (
                  <Row style={{ margin: "10px 0px 10px 0px", padding: "0px" }}>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <div
                        style={{ textAlign: 'left' }}
                        className={isFormSubmitError?"form_error_message":"form_success_message"}
                      >
                        {signUpSuccessMsg}
                      </div>
                    </Col>
                  </Row>
                )}
              <Row style={{ margin: "0px", padding: "0px" }}>
                <Col
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  xl={12}
                >
                  <div onClick={() => redirectPage('/signin')} style={{ textAlign: 'left', margin: '20px 0px 0px 0px' }}><div>Already have an account ? <b style={{ fontWeight: 'bolder', cursor: 'pointer' }}>Login</b></div></div>                  
                </Col>
              </Row>
          </Form>
        </Col>
      </Row>
  );
}

export default withRouter(Registration);

