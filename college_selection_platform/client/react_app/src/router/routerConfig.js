import Layout from '../layouts/Layout'
import Home from '../pages/Home';
import IndexPage from '../pages/IndexPage';
import Institutions from '../pages/Institutions';
import Main from '../pages/Main'
import Profile from '../pages/Profile';
import InstitutionDetails from '../pages/InstitutionDetails';
import AdminDashboard from '../pages/AdminDashboard'

const routes = [
  {
    path: "/",
    component: Institutions,
    layout: Layout,
    exact: true,
    is_auth:false,
    name: "main"
  },
  // {
  //   path: "/",
  //   layout: Layout,
  //   component: IndexPage,
  //   exact: true,
  //   is_auth:false,
  //   name: "index"
  // },
  {
    path: "/signin",
    layout: Layout,
    component: IndexPage,
    exact: true,
    is_auth:false,
    name: "index"
  },
  {
    path: "/signup",
    layout: Layout,
    component: IndexPage,
    exact: true,
    is_auth:false,
    name: "index",
  },
  {
    path: "/forgot_password",
    component: IndexPage,
    layout: Layout,
    exact: true,
    is_auth:false,
    name: "forgot_password",
  },
  {
    path: "/home",
    component: Home,
    layout: Layout,
    exact: true,
    is_auth:true,
    name: "home"
  },
  {
    path: "/institutions",
    component: Institutions,
    layout: Layout,
    exact: true,
    is_auth:false,
    name: "institutions"
  },
  {
    path: "/institutions/:id",
    component: InstitutionDetails,
    layout: Layout,
    is_auth: false,
    exact: false,
    name: "institution_details",
  },
  {
    path: "/profile",
    component: Profile,
    layout: Layout,
    exact: true,
    is_auth:true,
    name: "profile"
  },
  {
    path: "/admin",
    component: IndexPage,
    layout: Layout,
    exact: true,
    is_auth:false,
    name: "admin"
  },
  {
    path: "/admin_dashboard",
    component: AdminDashboard,
    layout: Layout,
    exact: true,
    is_auth:false,
    name: "admin"
  },
  {
    path: "/admin_dashboard/users/:id",
    component: Profile,
    layout: Layout,
    exact: true,
    is_auth:false,
    name: "admin"
  }
];

export default routes;
