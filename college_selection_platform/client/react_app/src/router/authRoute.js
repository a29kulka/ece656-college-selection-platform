import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import decode from "jwt-decode";
import {
  clear_session,
  sign_in,
  user_data,
} from "../redux";
import axiosInstance from "../components/AxiosInstance";
import { API_URL } from "../constants"

class AuthRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuth: false,
      curr_user_data: {},
    };
  }

  async componentDidMount() {
    let { history, auth, location, session, layout } = this.props;
    let isLoggedIn = this.props.session.isLoggedIn;
    // if (
    //   !location.pathname.includes('home') && !location.pathname.includes('admin')) {
    //   this.setState({ isAuth: true });
    // } else {
      if (auth) {
        
          if (isLoggedIn) {
            this.setState({ isAuth: true, curr_user_data: this.props.session.userData });
          } else {
            let id = localStorage.getItem("userId");
            const resp = await axiosInstance.get(API_URL+"is_authenticated/"+id+"/");
            const user = await resp.data.user;
            const ok = await resp.data.ok;
            this.setState({ curr_user_data: user });
            if (ok) {
              this.props.dispatch(sign_in());
              this.props.dispatch(user_data(user));
              this.setState({ isAuth: true });
            } else {
              history.push("/");
              this.props.dispatch(clear_session())
            }
          }
        } else {
          this.setState({ isAuth: true });
        }
    // }
  }

  render() {
    return (
      <div style={{ padding: "0px", margin: "0px" }}>
        {this.state.isAuth?(this.props.layout ? React.createElement(this.props.layout, null, React.createElement(this.props.component)) : React.createElement(this.props.component)):""}
      </div> )
    }
}

const mapStateToProps = state => ({
    session: state.session
});


export default withRouter(connect(mapStateToProps)(AuthRoute));
